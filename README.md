# Blazaar
Blazaar is a web application that can store an astronomical amount of data in a compact space.

> Blazaar V4xx is available here: https://bitbucket.org/Nils85/blazaar4

How it works?  
The home page allows you to send a file, which generates an unique link (URL) and a code to download this file at any time. Without this URL and the code nobody will be able to recover the file.

In background, files are [deduplicated](https://en.wikipedia.org/wiki/Data_deduplication) in blocks of fixed size (2KB). Each block is identified with an unique hash and then stored in a database. The probability of finding common blocks increases with the number of files stored.

Blocks added in the database are automatically mixed between 258 tables. Hashes of blocks are used to forming an hash list to reconstitute the original file. This hash list is encrypted. Only the person who kept the link generated after the upload of the file can decrypt the hash list with the code to download his file.

## Quick install
- Set up a web server with PHP and any database supported by PDO
- Choose a directory where uploaded files are stored and allow read/write access
- Copy PHP files of this project in your web server directory ("htdocs" or "www")
- Modify "CONFIG.php.txt" and rename it to "CONFIG.php"
- Modify "XSLTemplate/CONFIG.xsl.txt" and rename it to "CONFIG.xsl"
- Open a browser in the root domain of your web server
- Upload, download, store and share all your files!

## Wiki
- [in English](https://bitbucket.org/Nils85/blazaar3/wiki)
- [in French](https://bitbucket.org/Nils85/blazaar3/wiki/fr/Home)

## License
Blazaar is released under the terms of the AGPL-3.0 license. See the LICENSE.txt file for more information about GNU Affero General Public License v3.0