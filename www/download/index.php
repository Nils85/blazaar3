<?php
use XSLTemplate\XSLTemplate;
require '../CONFIG.php';

function __autoload($class_name)
{ require '../' . str_replace('\\', '/', $class_name) . '.php'; }

$blazar = new Blazar();
$deduplicator = $blazar->getDeduplicator();

if (filter_has_var(INPUT_POST, 'dl') && filter_has_var(INPUT_POST, 'code'))  // Download
{
	if ($deduplicator->download(filter_input(INPUT_POST,'dl'), filter_input(INPUT_POST,'code')))
	{ exit; }
}

$template = new XSLTemplate(Config\DEFAULT_LANGUAGE);
$template->xml = '<j/>';  // Download form

$stats = $blazar->getStats();
$template->rootAttributes = ['z' => $stats['Size'], 'y' => $stats['Files']];
$template->sendXML();