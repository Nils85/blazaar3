<?php
use XSLTemplate\XSLTemplate;
require '../CONFIG.php';

function __autoload($class_name)
{ require '../' . str_replace('\\', '/', $class_name) . '.php'; }

$blazar = new Blazar();
$deduplicator = $blazar->getDeduplicator();
$template = new XSLTemplate(Config\DEFAULT_LANGUAGE);
$template->xml = '<j/>';

if (filter_has_var(INPUT_POST, 'name'))
{
	$link = $deduplicator->copy(filter_input(INPUT_POST, 'name'),
		filter_input(INPUT_POST, 'link'), filter_input(INPUT_POST, 'code'));

	$infos = $deduplicator->getFileInfos($link['ID'], $link['Code']);

	if (isset($infos['Size']))
	{
		$blazar->incrementFilesStat($infos['Size']);
		$template->xml = '<c z="' . $link['ID'] . '" y="' . $link['Code'] . '">'
			. htmlspecialchars($infos['Name']) . '</c>';
	}
}
elseif (filter_has_var(INPUT_POST, 'link'))
{
	$link = filter_input(INPUT_POST, 'link');
	$code = filter_input(INPUT_POST, 'code');
	$file = $deduplicator->getFileInfos($link, $code);

	if (isset($file['Size']))
	{
		$template->xml = '<h z="' . $link . '" y="' . $file['Size'] . '" x="' . $code . '">'
			. htmlspecialchars($file['Name']) . '</h>';
	}
}

$stats = $blazar->getStats();
$template->rootAttributes = ['z' => $stats['Size'], 'y' => $stats['Files']];
$template->sendXML();