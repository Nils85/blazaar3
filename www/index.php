<?php
use XSLTemplate\XSLTemplate;
require 'CONFIG.php';

function __autoload($class_name)
{ require str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php'; }

$blazar = new Blazar();
$template = new XSLTemplate(Config\DEFAULT_LANGUAGE);

// Homepage
$template->xml = '<b z="' . Blazar\Blazar::getMaxUploadSize() . '">' . filter_input(INPUT_SERVER, 'HTTPS') . '</b>';

if (filter_has_var(INPUT_GET, 'dl'))  // Link
{
	if (filter_has_var(INPUT_POST, 'code'))
	{
		$link = filter_input(INPUT_GET, 'dl');
		$code = filter_input(INPUT_POST, 'code');
		$deduplicator = $blazar->getDeduplicator();
		$file = $deduplicator->getFileInfos($link, $code);

		if (isset($file['Size']))
		{
			$template->xml = '<d ';

			if (isset($file['Hash2']))
			{
				$template->xml .= 't="' . bin2hex($file['Hash3'])
					. '" u="' . bin2hex($file['Hash2']) . '" ';
			}

			$template->xml .= 'v="' . bin2hex($file['Target'])
				. '" w="' . $file['Download']
				. '" x="' . date('c', $file['Upload'])
				. '" q="' . date('c', $file['Access'])
				. '" z="' . $link
				. '" r="' . $code
				. '" y="' . $file['Size']
				. '" s="' . Blazar::getTimeLeft($file['Remove']) . '">' .
				htmlspecialchars($file['Name']) . '</d>';
		}
		else
		{
			$template->xml = '<f z="2">Not Found</f>';
		}
	}
	else
	{
		$template->xml = '<a/>';
	}
}

$stats = $blazar->getStats();
$template->rootAttributes = ['z' => $stats['Size'], 'y' => $stats['Files']];
$template->sendXML();