<?php
require 'CONFIG.php';

function __autoload($class_name)
{ require str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php'; }

$blazar = new Blazar();
$deduplicator = $blazar->getDeduplicator();
$path_folder = $deduplicator->getPathFolder();
$path_collision = $deduplicator->getPathCollision();

ignore_user_abort(true);
set_time_limit(0);

if (!is_dir($path_folder) || !is_dir($path_collision))
{ exit; }

$path = '';
$min_size = PHP_INT_MAX;

foreach (scandir($path_folder, SCANDIR_SORT_NONE) as $filename)
{
	$filepath = $path_folder . $filename;

	if (strlen($filename) > 9 && is_file($filepath))  // strlen('error.log') = 9
	{
		$size = filesize($filepath);

		if ($size > 0 && $size < $min_size)
		{
			$path = $filepath;
			$min_size = $size;
		}
	}
}

try
{
	if ($path != '' && !$deduplicator->partialFileExist())
	{
		$deduplicator->storeFile($path);

		while (file_exists($path) && !unlink($path))
		{ sleep(1); }
	}
}
catch (CollisionException $e)
{
	trigger_error($e);
	trigger_error($e->getBlock());

	while (file_exists($path) && !rename($path, $path_collision . basename($path)))
	{ sleep(1); }
}