<?php
require '../CONFIG.php';

function __autoload($class_name)
{ require '../' . str_replace('\\', '/', $class_name) . '.php'; }

$blazar = new Blazar();
$link = 'No file uploaded';

if (isset($_FILES['up']))  // Upload
{
	// Avoid a browser timeout with big upload
	if (ob_get_length())
	{
		ob_flush();
		flush();
	}

	if (is_array($_FILES['up']['tmp_name']))
	{
		$filename = $_FILES['up']['name'][0];
		$path = $_FILES['up']['tmp_name'][0];
	}
	else
	{
		$filename = $_FILES['up']['name'];
		$path = $_FILES['up']['tmp_name'];
	}

	$filesize = filesize($path);
	$deduplicator = $blazar->getDeduplicator();
	$link = $deduplicator->putFile($path, $filename);

	if ($link === array())
	{ $link = 'Crappy file'; }
	else
	{ $blazar->incrementFilesStat($filesize); }
}

header('Content-Type: text/plain');
echo '<p>#1<br/>';
echo 'Link : <input type="text" value="#0?dl=', $link['ID'];
echo '" style="width:250px" readonly="" onclick="this.select()"/>';
echo '</br>Code: <input type="text" value="', $link['Code'];
echo '" style="width:250px" readonly="" onclick="this.select()"/>';
echo '</p><a href="/upload/" style="text-decoration:underline">#2</a>';