<?php
use XSLTemplate\XSLTemplate;
require '../CONFIG.php';

function __autoload($class_name)
{ require '../' . str_replace('\\', '/', $class_name) . '.php'; }

$blazar = new Blazar();
$template = new XSLTemplate(Config\DEFAULT_LANGUAGE);
$template->xml = '<b z="' . ini_get('upload_max_filesize') . '">' . filter_input(INPUT_SERVER, 'HTTPS') . '</b>';

if (isset($_FILES['up']))  // Upload
{
	// Avoid a browser timeout with big upload
	if (ob_get_length())
	{
		ob_flush();
		flush();
	}

	if (is_array($_FILES['up']['tmp_name']))
	{
		$filename = $_FILES['up']['name'][0];
		$path = $_FILES['up']['tmp_name'][0];
	}
	else
	{
		$filename = $_FILES['up']['name'];
		$path = $_FILES['up']['tmp_name'];
	}

	$filesize = filesize($path);
	$deduplicator = $blazar->getDeduplicator();
	$link = $deduplicator->putFile($path, $filename);

	if ($link === '')
	{
		$template->xml = '<f>Crappy file</f>';
	}
	else
	{
		$template->xml = '<c z="' . $link . '">' . htmlspecialchars($filename) . '</c>';
		$blazar->incrementFilesStat($filesize);
	}
}

$stats = $blazar->getStats();
$template->rootAttributes = ['z' => $stats['Size'], 'y' => $stats['Files']];
$template->sendXML();