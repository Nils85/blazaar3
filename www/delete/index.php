<?php
use XSLTemplate\XSLTemplate;
require '../CONFIG.php';

function __autoload($class_name)
{ require '../' . str_replace('\\', '/', $class_name) . '.php'; }

$blazar = new Blazar();
$deduplicator = $blazar->getDeduplicator();
$template = new XSLTemplate(Config\DEFAULT_LANGUAGE);
$template->xml = '<k/>';

if (filter_has_var(INPUT_POST, 'link'))
{
	$code = filter_input(INPUT_POST, 'code');
	$link = filter_input(INPUT_POST, 'link');
	$position = strpos($link, '=');

	if ($position)
	{ $link = substr($link, $position +1); }

	$file = $deduplicator->getFileInfos($link, $code);

	if (isset($file['Size']))
	{
		$template->xml = '<i ';

		if (isset($file['Hash2']))
		{ $template->xml .= 't="' . bin2hex($file['Hash3']) . '" u="' . bin2hex($file['Hash2']) . '" '; }

		$template->xml .= 'z="' . $link
			. '" y="' . $file['Size']
			. '" w="' . $file['Download']
			. '" x="' . date('c',$file['Upload'])
			. '" v="' . bin2hex($file['Target'])
			. '" s="' . Blazar::getTimeLeft($file['Remove'])
			. '" q="' . date('c', $file['Access'])
			. '" r="' . $code . '">'
			. htmlspecialchars($file['Name']) . '</i>';
	}
	else
	{
		$template->xml = '<f z="2">Not Found</f>';
	}
}
elseif (filter_has_var(INPUT_POST, 'drop'))
{
	if ($deduplicator->delete(
		filter_input(INPUT_POST, 'drop'),
		filter_input(INPUT_POST, 'code'), Config\LINK_DROP_DELAY * 3600))
	{
		$template->xml = '<m z="1">' . Config\LINK_DROP_DELAY . '</m>';
	}
	else
	{
		$template->xml = '<f z="2">Not Found</f>';
	}
}

$stats = $blazar->getStats();
$template->rootAttributes = ['z' => $stats['Size'], 'y' => $stats['Files']];
$template->sendXML();