<?php
namespace Deduplicator;

/**
 * Tool for cryptography in real counter mode.
 * @uses OpenSSL
 */
class Crypto
{
	// Standard mode
	const CIPHER = 'AES-128-CFB';
	const KEY_ALGO = 'md5';  // 16 bytes
	const IV_ALGO = 'md5';  // 16 bytes

	// Secure mode
	//const CIPHER = 'AES-256-CFB';
	//const KEY_ALGO = 'ripemd256';  // 32 bytes
	//const IV_ALGO = 'md5';  // 16 bytes

	// Fast mode
	//const CIPHER = 'CAST5-CFB';
	//const KEY_ALGO = 'md5';  // 16 bytes
	//const IV_ALGO = 'fnv164';  // 8 bytes

	private $key;
	private $counter;

	function __construct($key, $counter = 0)
	{
		$this->counter = $counter;
		$this->key = hash(self::KEY_ALGO, $key, true);
	}

	public function encryptStream($data)
	{
		return openssl_encrypt($data, self::CIPHER, $this->key, OPENSSL_RAW_DATA,
			hash(self::IV_ALGO, $this->counter++, true));
	}

	public function decryptStream($data)
	{
		return openssl_decrypt($data, self::CIPHER, $this->key, OPENSSL_RAW_DATA,
			hash(self::IV_ALGO, $this->counter++, true));
	}

	static function encryptString($string, $key, $iv)
	{
		return openssl_encrypt($string, self::CIPHER, hash(self::KEY_ALGO, $key, true),
			OPENSSL_RAW_DATA, hash(self::IV_ALGO, $iv, true));
	}

	static function decryptString($string, $key, $iv)
	{
		return openssl_decrypt($string, self::CIPHER, hash(self::KEY_ALGO, $key, true),
			OPENSSL_RAW_DATA, hash(self::IV_ALGO, $iv, true));
	}

	static function benchmark($cipher, $test)
	{
		if (!function_exists('openssl_encrypt'))
		{ return 0; }

		$cipher = strtoupper($cipher);

		switch ($cipher)
		{
			case 'AES-128-CFB': $algo_key = 'md5'; $iv = 'md5'; break;  // Key & IV 16 bytes
			case 'AES-256-CFB': $algo_key = 'ripemd256'; $algo_iv = 'md5'; break;  // Key 32 bytes + IV 16 bytes
			case 'CAST5-CFB': $algo_key = 'md5'; $algo_iv = 'fnv164'; break;  // Key 16 bytes + IV 8 bytes
			default: return 0;
		}

		$key = hash($algo_key, $test, true);
		$iv = hash($algo_iv, $test, true);
		$encrypted = openssl_encrypt($test, $cipher, $key, OPENSSL_RAW_DATA, $iv);
		$start = microtime(true);

		for ($i=0; $i < 1000; ++$i)
		{ $decrypted = openssl_decrypt($encrypted, $cipher, $key, OPENSSL_RAW_DATA, $iv); }

		if ($decrypted !== $test)
		{ return 0; }

		return microtime(true) - $start;
	}
}