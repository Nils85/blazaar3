<?php
namespace Deduplicator;

/**
 * Read an existing hash list.
 */
class StreamReader
{
	private $file;
	private $bufferSize;
	private $crypto;
	private $key;

	function __construct($path, $chunk_size, $key)
	{
		$this->key = $key;
		$this->crypto = new Crypto($key);
		$this->bufferSize = $chunk_size;
		$this->file = fopen($path, 'rb');  // read only
		fseek($this->file, StreamStorage::METADATA_LENGTH);
	}

	public function read()
	{
		$data = fread($this->file, $this->bufferSize);

		if ($data === false)
		{ return ''; }

		return $this->crypto->decryptStream($data);
	}

	public function readEnd()
	{
		$position = ftell($this->file);

		fseek($this->file, -$this->bufferSize, SEEK_END);
		$counter = (ftell($this->file) - StreamStorage::METADATA_LENGTH) / $this->bufferSize;
		$data = fread($this->file, $this->bufferSize);

		$debug = bin2hex($data);

		fseek($this->file, $position);

		return Crypto::decryptString($data, $this->key, $counter);
	}

	public function getMetadata()
	{
		$position = ftell($this->file);
		rewind($this->file);

		$data = fread($this->file, StreamStorage::METADATA_LENGTH);
		fseek($this->file, $position);

		return Crypto::decryptString($data, $this->key, -1);
	}
}