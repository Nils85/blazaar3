<?php
namespace Deduplicator;
use DatabaseToolkit\DatabaseToolkit;
use finfo;
use Exception;

/**
 * File deduplicator.
 */
class Deduplicator
{
	const KEY_LENGTH = 22;
	const ALGO_HASH1 = 'SHA256';
	const ALGO_HASH2 = 'SHA1';
	const ALGO_HASH3 = 'MD5';
	const ALGO_CHECKSUM = 'crc32';
	const LENGTH_HASH2 = 20;

	private $pathFolder;
	private $pathDeduplication;
	private $pathCollision;
	private $storageLink;
	private $storageBlock;
	private $storageStream;

	public function __construct($folder_path, DatabaseToolkit &$database_instance, $tables_prefix = '')
	{
		date_default_timezone_set('UTC');

		$this->pathFolder = self::checkFolder($folder_path);
		$this->pathDeduplication = self::checkFolder($this->pathFolder . 'Deduplicated/');
		$this->pathCollision = self::checkFolder($this->pathFolder . 'Collisions/');

		$this->storageLink = new LinkStorage($database_instance, $tables_prefix);
		$this->storageBlock = new BlockStorage($database_instance, $tables_prefix);
		$this->storageStream = new StreamStorage(
			$this->pathDeduplication, BlockStorage::HASH1_LENGTH + BlockStorage::HASH2_LENGTH);
	}

	public function getPathFolder()
	{
		return $this->pathFolder;
	}

	public function getPathCollision()
	{
		return $this->pathCollision;
	}

	/**
	 * Put a new uploaded file.
	 * @param type $path Temp path of uploaded file
	 * @param type $name Uploaded filename
	 * @return string Link (ID + file key)
	 */
	public function putFile($path, $name)
	{
		$timestamp = time();
		$size = filesize($path);
		$name = str_replace(['\\', '/', ':', '*', '?', '<', '>', '|', '"',
			chr(0), chr(1), chr(2), chr(3), chr(4), chr(5), chr(6), chr(7), chr(8), chr(9), chr(10), chr(11),
			chr(12), chr(13), chr(14), chr(15), chr(16), chr(17), chr(18), chr(19), chr(20), chr(21), chr(22),
			chr(23), chr(24), chr(25), chr(26), chr(27), chr(28), chr(29), chr(30), chr(31), chr(127)],
				'', $name);

		if ($name == '' || $name == '.' || $name == '..' || $size < 1)
		{
			unlink($path);
			return '';
		}

		$checksum = hash_init(self::ALGO_CHECKSUM);
		$hash1 = hash_init(self::ALGO_HASH1);

		$file = fopen($path, 'rb');
		$block = fread($file, BlockStorage::BLOCK_SIZE);

		set_time_limit(300);  // Max response timeout for all browsers (5min)

		do
		{
			hash_update($hash1, $block);
			hash_update($checksum, $this->storageBlock->hash($block));

			$block = fread($file, BlockStorage::BLOCK_SIZE);
		}
		while ($block !== false && $block !== '');

		fclose($file);
		$hash = hash_final($hash1, true);
		$hex = bin2hex($hash);
		$new_path = $this->pathFolder . $hex;

		if (is_file($new_path) || $this->storageStream->exist($hex))
		{ unlink($path); }  // File already waiting or deduplicated
		else
		{ move_uploaded_file($path, $new_path); }  // ready to store blocks

		$key = self::randomPassword(self::KEY_LENGTH);
		$id = $this->storageLink->create($timestamp,
			Crypto::encryptString($hash, $key, $timestamp),
			Crypto::encryptString(hash_final($checksum, true), $key, $timestamp +1),
			Crypto::encryptString($name, $key, $timestamp +2));

		return ['ID' => $id, 'Code' => $key];
	}

	/**
	 * Get infos of file.
	 * @return array ID,Key,Name,Size,Created,Deleted,Counter,Target,Hash2,Hash3
	 */
	public function getFileInfos($id, $key)
	{
		$infos = $this->storageLink->find($id);

		if ($infos)
		{
			$infos['Target'] = Crypto::decryptString($infos['Target'], $key, $infos['Upload']);
			$infos['Checksum'] = Crypto::decryptString($infos['Checksum'], $key, $infos['Upload'] +1);
			$infos['Name'] = Crypto::decryptString($infos['Name'], $key, $infos['Upload'] +2);

			$hash = bin2hex($infos['Target']);
			$stream = $this->storageStream->open($hash, $infos['Checksum']);  // Key of stream
			$path = $this->pathFolder . $hash;

			if ($stream != null)
			{
				$metadata = $stream->getMetadata();
				$infos['Hash2'] = substr($metadata, 0, self::LENGTH_HASH2);
				$infos['Hash3'] = substr($metadata, self::LENGTH_HASH2);
				$infos['Size'] = ($this->storageStream->length($hash) -1) * BlockStorage::BLOCK_SIZE;
				$infos['Size'] += strlen($this->storageBlock->get($stream->readEnd()));
			}
			else if (is_file($path))
			{
				$infos['Size'] = filesize($path);
			}
		}

		return $infos;
	}

	public function download($link, $code)
	{
		$infos = $this->getFileInfos($link, $code);

		if (!isset($infos['Size']))
		{ return false; }

		$time_limit = $infos['Size'] / 6796;  // Min ~56 Kbit/s up to 700MB

		if ($time_limit > 108000)  // 30 hours
		{ $time_limit = $infos['Size'] / 14913; }  // Min ~128 Kbit/s up to 1.5GB

		// Filesize >1.5GB need download speed >128 Kbit/s
		set_time_limit($time_limit > 108000 ? 108000 : $time_limit);

		$infos['Target'] = bin2hex($infos['Target']);

		$this->storageLink->incrementCounter($infos['ID']);
		$hash_list = $this->storageStream->open($infos['Target'], $infos['Checksum']);

		if ($hash_list != null)
		{
			$hash = $hash_list->read();
			$block = $this->storageBlock->get($hash);

			self::downloadHeader($infos['Name'], $infos['Size'], $block);
			echo $block;

			while (($hash = $hash_list->read()) !== '')
			{
				$block = $this->storageBlock->get($hash);

				if ($block === '')
				{ throw new Exception('Unknown block: ' . bin2hex($hash)); }

				echo $block;
			}

			return true;
		}

		// else download file not deduplicated...
		$path = $this->pathCollision . $infos['Target'];

		if (!is_file($path))
		{ $path = $this->pathFolder . $infos['Target']; }

		return $this->downloadFile($path, $infos['Name'], $infos['Target'], $infos['Size']);
	}

	public function downloadFile($path, $name, $hash1, $size)
	{
		if (hash_file(self::ALGO_HASH1, $path) != $hash1)
		{ throw new Exception('Corrupted file: ' . $hash1); }

		$finfo = new finfo(FILEINFO_MIME);  // or FILEINFO_MIME_TYPE
		$mime = $finfo->file($path);

		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename="' . $name . '"');
		header('Content-Length: ' . $size);

		if ($mime && strpos($mime, 'x-empty') === false)
		{ header('Content-Type: ' . $mime); }

		readfile($path);
		return true;
	}

	static function downloadHeader($filename, $filesize, $chunk)
	{
		$finfo = new finfo(FILEINFO_MIME);  // or FILEINFO_MIME_TYPE
		$mime = $finfo->buffer($chunk);

		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		header('Content-Length: ' . $filesize);

		if ($mime && strpos($mime, 'x-empty') === false)
		{ header('Content-Type: ' . $mime); }

		//if (ob_get_length()) { ob_end_flush(); }
	}

	public function storeFile($path)
	{
		$hash1 = hash_init(self::ALGO_HASH1);
		$hash2 = hash_init(self::ALGO_HASH2);
		$hash3 = hash_init(self::ALGO_HASH3);
		$checksum = hash_init(self::ALGO_CHECKSUM);

		$file = fopen($path, 'rb');
		$block = fread($file, BlockStorage::BLOCK_SIZE);

		while ($block !== false && $block !== '')
		{
			hash_update($hash1, $block);
			hash_update($hash2, $block);
			hash_update($hash3, $block);
			hash_update($checksum, $this->storageBlock->hash($block));

			$block = fread($file, BlockStorage::BLOCK_SIZE);
		}

		$hash1 = hash_final($hash1, false);
		$checksum = hash_final($checksum, true);
		$stream = $this->storageStream->create($hash1, $checksum);  // key = checksum of stream

		if ($stream == null)
		{ throw new Exception('File already deduplicated: ' . $path); }

		$hash2 = hash_final($hash2, true);
		$hash3 = hash_final($hash3, true);

		rewind($file);
		$block = fread($file, BlockStorage::BLOCK_SIZE);

		while ($block !== false && $block !== '')
		{
			try
			{
				$hash = $this->storageBlock->put($block);
				$stream->write($hash);
			}
			catch (Exception $e)
			{
				fclose($file);
				$stream->drop();

				while (!rename($path, $this->pathCollision . basename($path)))
				{ sleep(1); }

				throw $e;
			}

			$block = fread($file, BlockStorage::BLOCK_SIZE);
		}

		fclose($file);
		$stream->store($hash2 . $hash3);
	}

	public function partialFileExist()
	{
		return $this->storageStream->partialHashExist();
	}

	public function totalPendingFiles()
	{
		$counters = ['File' => 0, 'Size' => 0];
		$path_folder = $this->getPathFolder();

		foreach (scandir($path_folder, SCANDIR_SORT_NONE) as $filename)
		{
			$filepath = $path_folder . $filename;

			if (is_file($filepath))
			{
				$counters['Size'] += filesize($filepath);
				++$counters['File'];
			}
		}

		return $counters;
	}

	public function totalBlock()
	{
		return $this->storageBlock->count();
	}

	public function totalBlockSize()
	{
		return $this->storageBlock->size();
	}

	public function totalLink()
	{
		return $this->storageLink->count();
	}

	public function totalHash()
	{
		return $this->storageStream->count();
	}

	public function totalHashLength()
	{
		return $this->storageStream->length();
	}

	public function copy($name = '', $link, $code)
	{
		$infos = $this->getFileInfos($link, $code);

		if (!isset($infos['Size']))
		{ return ''; }  // Bad key

		$timestamp = time();
		$name = str_replace(['\\', '/', ':', '*', '?', '<', '>', '|', '"',
			chr(0), chr(1), chr(2), chr(3), chr(4), chr(5), chr(6), chr(7), chr(8), chr(9), chr(10), chr(11),
			chr(12), chr(13), chr(14), chr(15), chr(16), chr(17), chr(18), chr(19), chr(20), chr(21), chr(22),
			chr(23), chr(24), chr(25), chr(26), chr(27), chr(28), chr(29), chr(30), chr(31), chr(127)],
				'', $name);

		if ($name == '' || $name == '.' || $name == '..')
		{ $name = $infos['Name']; }

		$key = self::randomPassword(self::KEY_LENGTH);
		$id = $this->storageLink->create($timestamp,
			Crypto::encryptString($infos['Target'], $key, $timestamp),
			Crypto::encryptString($infos['Checksum'], $key, $timestamp +1),
			Crypto::encryptString($name, $key, $timestamp +2));

		return ['ID' => $id, 'Code' => $key];
	}

	public function delete($link, $code, $second)
	{
		$infos = $this->getFileInfos($link, $code);

		if (isset($infos['Size']))
		{ return $this->storageLink->drop($infos['ID'], $second); }

		return false;  // Bad key
	}

	static function checkPath($path)
	{
		$last_char = substr($path, -1);

		if ($last_char != '/' && $last_char != '\\')
		{ return $path . DIRECTORY_SEPARATOR; }

		return $path;
	}

	static function checkFolder($path)
	{
		if (!is_dir($path))  // or not exists
		{
			if (!mkdir($path, 0777, true))
			{ throw new Exception('Unable to create folder ' . $path); }
		}

		return self::checkPath($path);
	}

	static function randomString($length)
	{
		$string = '';
		$base65 = str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWVXYZ') . '-_.';

		while (--$length > 0)
		{ $string .= $base65[mt_rand(0,64)]; }

		return $string . $base65[mt_rand(0,61)];  // Never end with a special char
	}

	static function randomPassword($length)
	{
		$password = '';
		$base62 = str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

		while (--$length >= 0)
		{ $password .= $base62[mt_rand(0,61)]; }

		return $password;
	}
}