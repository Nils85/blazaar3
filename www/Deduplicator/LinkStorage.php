<?php
namespace Deduplicator;
use DatabaseToolkit\DatabaseToolkit;
use DatabaseToolkit\Sql;
use PDO;

/**
 * Storage engine for links.
 */
class LinkStorage
{
	const ID_LENGTH = 7;

	private $db;
	private $table;

	function __construct(DatabaseToolkit &$database_instance, $tables_prefix = '')
	{
		$this->db = $database_instance;
		$this->table = $tables_prefix . 'Link';
	}

	public function create($unixtime, $hash, $checksum, $name)
	{
		$timestamp = time();
		$id = '';
		$sql = "insert into $this->table (Name,Upload,Target,Checksum,Access,ID) values (?,?,?,?,?,?)";
		$stmt = $this->db->pdo->prepare($sql);

		if ($stmt == false)  // or null
		{
			$this->createTable(strlen($hash), strlen($checksum));
			$stmt = $this->db->pdo->prepare($sql);
		}

		$stmt->bindParam(1, $name, PDO::PARAM_LOB);
		$stmt->bindParam(2, $unixtime, PDO::PARAM_INT);
		$stmt->bindParam(3, $hash, PDO::PARAM_LOB);
		$stmt->bindParam(4, $checksum, PDO::PARAM_LOB);
		$stmt->bindParam(5, $timestamp, PDO::PARAM_INT);
		$stmt->bindParam(6, $id, PDO::PARAM_STR);

		do { $id = Deduplicator::randomString(self::ID_LENGTH); }
		while (!$stmt->execute());

		return $id;
	}

	public function find($id)
	{
		$id = $this->db->pdo->quote($id);

		$stmt = $this->db->pdo->query(
			"select ID,Name,Upload,Access,Remove,Download,Target,Checksum from $this->table where ID = $id");

		if ($stmt == false)  // or null
		{ return false; }

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row)
		{
			if ($row['Remove'] != 0 && $row['Remove'] < time())
			{
				$this->db->pdo->exec("delete from $this->table where ID = $id");
				return false;
			}

			$row['Download'] += Sql::OFFSET_INT4;
			$this->db->pdo->exec('update '. $this->table . ' set Access=' . time() . ' where ID=' . $id);
		}

		return $row;
	}

	public function drop($id, $second)
	{
		$stmt = $this->db->pdo->prepare('update ' . $this->table . ' set Remove=? where ID=? and Remove=0');
		$stmt->bindValue(1, time() + $second, PDO::PARAM_INT);
		$stmt->bindValue(2, $id, PDO::PARAM_STR);
		return $stmt->execute();
	}

	public function incrementCounter($id)
	{
		$stmt = $this->db->pdo->prepare('update ' . $this->table . ' set Download=Download+1 where ID=?');
		$stmt->bindValue(1, $id, PDO::PARAM_STR);
		$stmt->execute();
	}

	public function count()
	{
		$stmt = $this->db->pdo->query('select count(*) from ' . $this->table);

		if ($stmt == false)  // or null
		{ return 0; }

		return $stmt->fetchColumn(0);
	}

	private function createTable($hash_size, $checksum_size)
	{
		$min = \DatabaseToolkit\Sql::OFFSET_INT4;
		$int = $this->db->sql->typeInteger(4);
		$bin = $this->db->sql->typeBinary($hash_size);
		$bin2 = $this->db->sql->typeBinary($checksum_size);
		$varbin = $this->db->sql->typeVarbinary(255);
		$char = 'char(' . self::ID_LENGTH . ')';

		$this->db->pdo->exec("create table $this->table ("
			. "ID $char primary key,"
			. "Upload $int not null,"
			. "Access $int not null,"
			. "Download $int not null default -$min ,"
			. "Remove $int not null default 0,"
			. "Target $bin not null,"
			. "Checksum $bin2 not null,"
			. "Name $varbin not null)");
	}
}