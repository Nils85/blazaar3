<?php
namespace Deduplicator;

class CollisionException extends Exception
{
	protected $block = '';
	protected $filename = '';

	public function __construct($block, $filename = '')
	{
		parent::__construct('Block collision', 0);
		$this->block = $block;
		$this->filename = $filename;
	}

	public function __toString()
	{
		$msg = $this->message;

		if($filename != '')
		{ $msg .= ' in file: ' . $filename; }

		return $msg;
	}

	public function getBlock()
	{
		return $block;
	}
}