<?php
namespace Deduplicator;

/**
 * Storage engine for hash list.
 */
class StreamStorage
{
	private $path;
	private $bufferSize;
	const METADATA_LENGTH = 36;  // Length of hash2 + hash3
	const TIMEOUT = 86400;  // 24 hours

	function __construct($folder_path, $chunk_size)
	{
		$this->path = Deduplicator::checkFolder($folder_path);
		$this->bufferSize = $chunk_size;
	}

	public function create($id, $key)
	{
		if (is_file($this->path . $id))
		{ return null; }

		return new StreamWriter($this->bufferSize, $this->path, $id, $key);
	}

	public function open($id, $key)
	{
		if (is_file($this->path . $id))
		{ return new StreamReader($this->path . $id, $this->bufferSize, $key); }

		return null;
	}

	public function length($id = "")
	{
		if ($id == "")  // or null
		{
			$counter = 0;

			foreach (scandir($this->path) as $filename)
			{
				if ($filename[0] != '.')
				{ $counter += $this->length($filename); }
			}

			return $counter;
		}

		$path = $this->path . $id;

		if (is_file($path))
		{ return (filesize($path) - self::METADATA_LENGTH) / $this->bufferSize; }

		return 0;
	}

	public function exist($id)
	{
		return is_file($this->path . $id);
	}

	public function count()
	{
		$counter = 0;

		foreach (scandir($this->path) as $filename)
		{
			if ($filename[0] != '.')
			{ ++$counter; }
		}

		return $counter;
	}

	public function partialHashExist()
	{
		foreach (scandir($this->path) as $filename)
		{
			if ($filename[0] == '.' && strlen($filename) > 2)  // ignore "." and ".."
			{
				if (time() < filemtime($this->path . $filename) + self::TIMEOUT)
				{ return true; }

				return !unlink($this->path . $filename);
			}
		}

		return false;
	}
}