<?php
namespace Deduplicator;
use DatabaseToolkit\DatabaseToolkit;
use PDO;
use Exception;

/**
 * Storage engine for data blocks.
 */
class BlockStorage
{
	const BLOCK_SIZE = 2048;
	const HASH1_ALGO = 'fnv164';
	const HASH1_LENGTH = 8;  // bytes
	const HASH2_ALGO = 'crc32b';
	const HASH2_LENGTH = 4;  // bytes

	private $db;
	private $tableShortKey;
	private $tableLongKey;
	private $tableSmallBlock;

	function __construct(DatabaseToolkit &$database_instance, $tables_prefix = '')
	{
		$this->db = $database_instance;
		$this->tableShortKey = $tables_prefix . 'Block_';
		$this->tableLongKey = $tables_prefix . 'Block';
		$this->tableSmallBlock = $tables_prefix . 'BlockSmall';
	}

	public function put($data)
	{
		$key = self::hash($data);
		$original_size = strlen($data);
		$compressed = gzdeflate($data, 9);

		if (strlen($compressed) < $original_size)
		{ $data = $compressed; }

		if ($original_size < self::BLOCK_SIZE)
		{
			if ($this->insertSmallBlock($key, $data))
			{ return $key; }

			if ($this->get($key) === $data)
			{ return $key; }

			throw new Exception('Unable to store ' . $original_size. 'B: ' . bin2hex($data));
		}

		if($this->insertHalfKey(substr($key, 0, self::HASH1_LENGTH), $data))
		{ return $key; }

		if ($this->get($key) !== '')
		{ return $key; }

		if ($this->insertFullKey($key, $data))
		{ return $key; }

		throw new Exception('Unable to store ' . $original_size. 'B: ' . bin2hex($data));
	}

	private function insertHalfKey($key, $data)
	{
		static $prepared_statements = array();

		if ($prepared_statements === array())
		{
			$i = 0;

			while ($i < 256)
			{
				$suffix = str_pad(dechex($i), 2, '0', STR_PAD_LEFT);

				$prepared_statement = $this->db->pdo->prepare(
					'insert into ' . $this->tableShortKey . $suffix . '(ID,Data) values (?,?)');

				if ($prepared_statement == false)  // or null
				{
					$this->createTables();
					continue;  // retry to prepare statement
				}

				$prepared_statements[hex2bin($suffix)] = $prepared_statement;
				++$i;
			}
		}

		$prepared_statement = $prepared_statements[$key[0]];

		// Don't use bindParam! it can cause segmentation fault with null bytes
		$prepared_statement->bindValue(1, substr($key,1), PDO::PARAM_LOB);
		$prepared_statement->bindValue(2, $data, PDO::PARAM_LOB);

		return $prepared_statement->execute();
	}

	private function insertFullKey($key, $data)
	{
		static $prepared_statement = false;

		if (!$prepared_statement)
		{ $prepared_statement = $this->db->pdo->prepare("insert into $this->tableLongKey (ID,Data) values (?,?)"); }

		$prepared_statement->bindValue(1, $key, PDO::PARAM_LOB);
		$prepared_statement->bindValue(2, $data, PDO::PARAM_LOB);

		return $prepared_statement->execute();
	}

	private function insertSmallBlock($key, $data)
	{
		$prepared_statement = $this->db->pdo->prepare("insert into $this->tableSmallBlock (ID,Data) values (?,?)");
		$prepared_statement->bindValue(1, $key, PDO::PARAM_LOB);
		$prepared_statement->bindValue(2, $data, PDO::PARAM_LOB);

		return $prepared_statement->execute();
	}

	public function get($key)
	{
		$data = $this->selectHalfKey(substr($key, 0, self::HASH1_LENGTH));

		if ($data !== '')
		{
			if (!isset($data[self::BLOCK_SIZE -1]))  // strlen($data) != self::BLOCK_SIZE
			{ $data = gzinflate($data); }

			if (self::hash($data) === $key)
			{ return $data; }
		}

		$data = $this->selectFullKey($key);

		if ($data !== '')
		{
			if (!isset($data[self::BLOCK_SIZE -1]))  // strlen($data) != self::BLOCK_SIZE
			{ $data = gzinflate($data); }

			if (self::hash($data) === $key)
			{ return $data; }

			throw new Exception('Corrupted block: ' . bin2hex($key));
		}

		$data = $this->selectSmallBlock($key);

		if ($data === '')
		{ return ''; }

		if (self::hash($data) === $key)
		{ return $data; }

		$data = gzinflate($data);

		if (self::hash($data) === $key)
		{ return $data; }

		throw new Exception('Corrupted block: ' . bin2hex($key));
	}

	private function selectHalfKey($key)
	{
		static $prepared_statements = array();

		if ($prepared_statements === array())
		{
			for ($i=0; $i < 256; ++$i)
			{
				$suffix = str_pad(dechex($i), 2, '0', STR_PAD_LEFT);

				$prepared_statement = $this->db->pdo->prepare(
					'select Data from ' . $this->tableShortKey . $suffix . ' where ID=?');

				$prepared_statements[hex2bin($suffix)] = $prepared_statement;
			}
		}

		$prepared_statement = $prepared_statements[$key[0]];
		$prepared_statement->bindValue(1, substr($key, 1), PDO::PARAM_LOB);
		$prepared_statement->execute();
		$rows = $prepared_statement->fetchAll(PDO::FETCH_COLUMN, 0);

		if (empty($rows))
		{ return ''; }

		return $rows[0];
	}

	private function selectFullKey($key)
	{
		static $prepared_statement = false;

		if (!$prepared_statement)
		{ $prepared_statement = $this->db->pdo->prepare("select Data from $this->tableLongKey where ID=?"); }

		$prepared_statement->bindValue(1, $key, PDO::PARAM_LOB);
		$prepared_statement->execute();
		$rows = $prepared_statement->fetchAll(PDO::FETCH_COLUMN, 0);

		if (empty($rows))
		{ return ''; }

		return $rows[0];
	}

	private function selectSmallBlock($key)
	{
		$prepared_statement = $this->db->pdo->prepare("select Data from $this->tableSmallBlock where ID=?");
		$prepared_statement->bindValue(1, $key, PDO::PARAM_LOB);
		$prepared_statement->execute();
		$rows = $prepared_statement->fetchAll(PDO::FETCH_COLUMN, 0);

		if (empty($rows))
		{ return ''; }

		return $rows[0];
	}

	private function createTables()
	{
		$id_type = $this->db->sql->typeBinary(self::HASH1_LENGTH -1);
		$data_type = $this->db->sql->typeVarbinary(self::BLOCK_SIZE);

		for ($i=0; $i < 256; ++$i)
		{
			$this->db->pdo->exec(
				'create table ' . $this->tableShortKey . str_pad(dechex($i), 2, '0', STR_PAD_LEFT)
				. '(ID ' . $id_type . ' primary key, Data ' . $data_type . ' not null)');
		}

		$id_type = $this->db->sql->typeBinary(self::HASH1_LENGTH + self::HASH2_LENGTH);

		$this->db->pdo->exec(
			"create table $this->tableLongKey (ID $id_type primary key, Data $data_type not null)");

		$data_type = $this->db->sql->typeVarbinary(self::BLOCK_SIZE -1);
		$this->db->pdo->exec(
			"create table $this->tableSmallBlock (ID $id_type primary key, Data $data_type not null)");
	}

	public function count()
	{
		$counter = 0;
		$stmt = $this->db->pdo->query('select count(*) from ' . $this->tableLongKey);

		if ($stmt == false)  // or null
		{ return 0; }

		$counter += $stmt->fetchAll(PDO::FETCH_COLUMN, 0)[0];

		$stmt = $this->db->pdo->query('select count(*) from ' . $this->tableSmallBlock);
		$counter += $stmt->fetchAll(PDO::FETCH_COLUMN, 0)[0];

		for ($i=0; $i < 256; ++$i)
		{
			$stmt = $this->db->pdo->query(
				'select count(*) from ' . $this->tableShortKey . str_pad(dechex($i), 2, '0', STR_PAD_LEFT));

			$counter += $stmt->fetchAll(PDO::FETCH_COLUMN, 0)[0];
		}

		return $counter;
	}

	public function size()
	{
		$counter = 0;
		$stmt = $this->db->pdo->query(
			'select sum(' . $this->db->sql->functionLength('Data') . ') from ' . $this->tableLongKey);

		if ($stmt == false)  // or null
		{ return 0; }

		$counter += $stmt->fetchAll(PDO::FETCH_COLUMN, 0)[0];

		$stmt = $this->db->pdo->query(
			'select sum(' . $this->db->sql->functionLength('Data') . ') from ' . $this->tableSmallBlock);
		$counter += $stmt->fetchAll(PDO::FETCH_COLUMN, 0)[0];

		for ($i=0; $i < 256; ++$i)
		{
			$stmt = $this->db->pdo->query(
				'select sum(' . $this->db->sql->functionLength('Data') . ') from '
					. $this->tableShortKey . str_pad(dechex($i), 2, '0', STR_PAD_LEFT));

			$counter += $stmt->fetchAll(PDO::FETCH_COLUMN, 0)[0];
		}

		return $counter;
	}

	static function hash($data)
	{
		return hash(self::HASH1_ALGO, $data, true) . hash(self::HASH2_ALGO, $data, true);
	}
}