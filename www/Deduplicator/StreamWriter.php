<?php
namespace Deduplicator;

/**
 * Build a new hash list.
 */
class StreamWriter
{
	private $file;
	private $filename;
	private $folder;
	private $bufferSize;
	private $crypto;
	private $key;

	function __construct($chunk_size, $folder, $filename, $key)
	{
		$this->key = $key;
		$this->crypto = new Crypto($key);
		$this->bufferSize = $chunk_size;
		$this->filename = $filename;
		$this->folder = $folder;
		$this->file = fopen($folder . '.' . $filename, 'wb');  // if file exist truncate else create new
		fwrite($this->file,
			str_repeat(hex2bin('00'), StreamStorage::METADATA_LENGTH));
	}

	public function write($data)
	{
		if (fwrite($this->file, $this->crypto->encryptStream($data)) != $this->bufferSize)
		{ throw new Exception('Problem with data ' . bin2hex($data) . ' in stream ' . $this->filename); }
	}

	public function store($metadata)
	{
		rewind($this->file);

		if (fwrite($this->file, Crypto::encryptString($metadata, $this->key, -1)) != StreamStorage::METADATA_LENGTH)
		{ throw new Exception('Problem with metadata ' . bin2hex($metadata) . ' in stream ' . $this->filename); }

		fclose($this->file);

		$old_path = $this->folder . '.' . $this->filename;
		$new_path = $this->folder . $this->filename;

		if (is_file($new_path))
		{ unlink($old_path); }
		else
		{ rename($old_path, $new_path); }
	}

	public function drop()
	{
		fclose($this->file);
		unlink($this->folder . '.' . $this->filename);
	}
}