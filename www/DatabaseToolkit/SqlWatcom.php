<?php
namespace DatabaseToolkit;

/**
 * Watcom-SQL: SAP Sybase SQL Anywhere v10+
 * @author Vince
 */
class WatcomSql extends Sql
{
	static function typeBinary($length)
	{
		if ($length > 32767)
		{ return 'long binary'; }

		return "binary($length)";
	}

	static function typeVarbinary($max_length)
	{
		if ($max_length > 32767)
		{ return 'long binary'; }

		return "varbinary($max_length)";
	}

	static function typeInteger($size)
	{
		switch ($size)
		{
			case 1: return 'tinyint';
			case 2: return 'smallint';
			case 3:
			case 4: return 'int';
		}

		return 'bigint';
	}
}