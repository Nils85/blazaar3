<?php
namespace DatabaseToolkit;

/**
 * PL/SQL: Oracle Database 9i+ (v9.0.1+)
 * @author Vince
 */
class PlSql extends Sql
{
	static function typeInteger($size)
	{
		switch ($size)
		{
			case 1: return 'number(3)';
			case 2: return 'number(5)';
			case 3: return 'number(7)';
			case 4: return 'number(10)';
			case 5: return 'number(12)';
			case 6: return 'number(15)';
			case 7: return 'number(17)';
		}

		return 'number(19)';
	}
}