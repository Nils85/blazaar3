<?php
namespace DatabaseToolkit;

/**
 * Generic SQL: high compatibility or standard.
 * @todo function getTypeReal($total_digit, $digit_after_point)
 * @todo function getTypeFloat($precision, $scale)
 */
class Sql
{
	// Offset to convert a signed integer to unsigned, or vice versa
	// Useful with increment function
	const OFFSET_INT1 = 0;  // Negative tinyint are incompatible with MS SQL Server
	const OFFSET_INT2 = 32767;
	const OFFSET_INT3 = 8388608;
	const OFFSET_INT4 = 2147483647;
	const OFFSET_INT5 = 999999999999;
	const OFFSET_INT6 = 999999999999999;
	const OFFSET_INT7 = 99999999999999999;
	const OFFSET_INT8 = 9223372036854775807;

	static function typeBinary($length)
	{
		return 'blob';
	}

	static function typeVarbinary($max_length)
	{
		return 'blob';
	}

	static function typeChar($length)
	{
		return "char($length)";
	}

	static function typeVarchar($max_length)
	{
		return "varchar($max_length)";
	}

	static function typeInteger($size)
	{
		switch ($size)
		{
			case 1:
			case 2: return 'smallint';
			case 3:
			case 4: return 'int';
		}

		return 'bigint';
	}

	/**
	 * Increment an integer manually or start with smallest (negative) number.
	 * Replace AUTO_INCREMENT, IDENTITY or SERIAL because is really not standard in all databases...
	 * Sometimes it doesn't exist (Oracle) or too complex (Firebird with trigger)
	 * @example "insert into Table (ID) select " . increment("ID") . " from Table"
	 * @param string $column_name Name of column to increment
	 * @param string|int $min_value Minimal value (default = INT4)
	 * @return string SQL functions
	 */
	static function increment($column_name, $min_value = self::OFFSET_INT4)
	{
		return 'coalesce(max(' . $column_name . ')+1,-' . $min_value . ')';
	}

	static function functionLength($column_name)
	{
		return "length($column_name)";
	}
}