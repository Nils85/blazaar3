<?php
namespace DatabaseToolkit;

/**
 * SPL: IBM Informix v10+ & Teradata
 * @author Vince
 */
class Spl extends Sql
{
	static function typeInteger($size)
	{
		switch ($size)
		{
			case 1:
			case 2: return 'smallint';
			case 3:
			case 4: return 'int';
		}

		return 'int8';
	}

	static function increment($column_name, $min_value = DatabaseAbstraction::OFFSET_INT4)
	{
		return "nvl(max($column_name)+1,-$min_value)";
	}
}