<?php
namespace DatabaseToolkit;

/**
 * T-SQL: Sybase Microsoft SQL Server 2000+ (v8.0+)
 */
class TransactSql extends Sql
{
	static function typeBinary($length)
	{
		if ($length > 8000)
		{ return 'varbinary(max)'; }

		return "binary($length)";
	}

	static function typeVarbinary($max_length)
	{
		if ($max_length > 8000)
		{ return 'varbinary(max)'; }

		return "varbinary($max_length)";
	}

	static function typeInteger($size)
	{
		switch ($size)
		{
			case 1: return 'tinyint';
			case 2: return 'smallint';
			case 3:
			case 4: return 'int';
		}

		return 'bigint';
	}

	static function functionLength($column_name)
	{
		return "len($column_name)";
	}
}