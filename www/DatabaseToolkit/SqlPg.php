<?php
namespace DatabaseToolkit;

/**
 * PL/pgSQL: PostgreSQL v6.5+
 * @author Vince
 */
class PgSql extends Sql
{
	static function typeBinary($length)
	{
		return 'bytea';
	}

	static function typeVarbinary($max_length)
	{
		return 'bytea';
	}
}