<?php
namespace DatabaseToolkit;
use PDO;
use PDOException;

class DatabaseToolkit
{
	public $sql;
	public $pdo;
	public $type;

	/**
	 * Construct SQL+PDO instances and create database if not exists automatically (like SQLite).
	 * @param string $dsn Data Source Name
	 * @param string $username
	 * @param string $password
	 * @param array $options PDO options
	 */
	function __construct($dsn, $username = null, $password = null, $options = array())
	{
		try
		{
			$this->pdo = new PDO($dsn, $username, $password, $options);
		}
		catch (PDOException $e)
		{
			self::createDatabase($dsn, $username, $password);
			$this->pdo = new PDO($dsn, $username, $password, $options);
		}

		$this->type = self::systemName($dsn);
		$this->sql = self::getSqlToolbox($this->type);
	}

	/**
	 * Create database with PDO DSN.
	 * @param string $dsn Data Source Name
	 * @param string $username
	 * @param string $password
	 */
	static function createDatabase($dsn, $username, $password)
	{
		$dsn = explode(':', $dsn);
		$driver = $dsn[0];
		$connection_string = '';
		$database_name = '';

		if (substr($driver,0,6) != 'sqlite')
		{
			foreach (explode(';', $dsn[1]) as $param)
			{
				if (substr($param,0,7) === 'dbname=')
				{ $database_name = substr($param, 7); }
				elseif (substr($param,0,9) === 'database=')
				{ $database_name = substr($param, 9); }
				else
				{ $connection_string .= $param . ';'; }
			}

			if ($database_name != '')
			{
				$connection_string = substr($connection_string, 0, -1);

				$pdo = new PDO($driver . ':' . $connection_string, $username, $password);
				$pdo->exec('create database ' . $database_name);
			}
		}
	}

	static function getSqlToolbox($database_system)
	{
		switch ($database_system)
		{
			case 'mysql':    return new SqlPsm();
			case 'pgsql':    return new PgSql();
			case 'oci':      return new PlSql();
			case 'mssql':    return new TransactSql();
			case 'sybase':   return new WatcomSql();
			case 'informix': return new Spl();
		}

		return new Sql();  // Sqlite, Cubrid and other...
	}

	/**
	 * Detect the database system used in PDO or ODBC connection string.
	 * @param string $dsn Data Source Name (PDO)
	 * @return string sqlite mysql pgsql oci mssql sybase firebird ibm informix cubrid
	 */
	static function systemName($dsn)
	{
		$dsn = strtolower($dsn);
		$colon_position = strpos($dsn, ':');
		$driver_name = substr($dsn, 0, $colon_position);

		if ($driver_name == 'odbc')
		{
			foreach (explode(';', substr($dsn, $colon_position)) as $param)
			{
				if (substr($param,0,7) === 'driver=')
				{ $driver_name = substr($param,7); }
			}
		}

		if ($driver_name != '' && $driver_name[0] === '{')  // ODBC
		{
			if (strpos($driver_name, 'sqlite')) { return 'sqlite'; }
			if (strpos($driver_name, 'mysql')) { return 'mysql'; }
			if (strpos($driver_name, 'postgres')) { return 'pgsql'; }
			if (strpos($driver_name, 'oracle')) { return 'oci'; }
			if (strpos($driver_name, 'informix')) { return 'informix'; }
			if (strpos($driver_name, 'sql server') || strpos($driver_name, 'sql native')) { return 'mssql'; }
			if (strpos($driver_name, 'sybase') || strpos($driver_name, 'freetds')) { return 'sybase'; }
			if (strpos($driver_name, 'firebird') || strpos($driver_name, 'interbase')) { return 'firebird'; }
			if (strpos($driver_name, 'ibm') || strpos($driver_name, 'db2')) { return 'ibm'; }
			if (strpos($driver_name, 'cubrid')) { return 'cubrid'; }
		}

		switch ($driver_name)
		{
			case 'sqlite2': return 'sqlite';
			case 'dblib':   return 'sybase';
			case 'sqlsrv':  return 'mssql';
		}

		return $driver_name;
	}
}