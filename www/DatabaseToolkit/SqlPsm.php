<?php
namespace DatabaseToolkit;

/**
 * SQL/PSM: MySQL v3.23.3+, MariaDB, Drizzle...
 */
class SqlPsm extends Sql
{
	static function typeBinary($length)
	{
		if ($length > 16777215)
		{ return 'longblob'; }

		if ($length > 65535)
		{ return 'mediumblob'; }

		if ($length > 255)
		{ return "varbinary($length)"; }

		return "binary($length)";
	}

	static function typeVarbinary($max_length)
	{
		if ($max_length > 16777215)
		{ return 'longblob'; }

		if ($max_length > 65535)
		{ return 'mediumblob'; }

		return "varbinary($max_length)";
	}

	static function typeInteger($size)
	{
		switch ($size)
		{
			case 1: return 'tinyint';
			case 2: return 'smallint';
			case 3: return 'mediumint';
			case 4: return 'int';
		}

		return 'bigint';
	}
}