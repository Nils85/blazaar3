<?php
use XSLTemplate\XSLTemplate;
require '../CONFIG.php';

function __autoload($class_name)
{ require '../' . str_replace('\\', '/', $class_name) . '.php'; }

$blazar = new Blazar();
$deduplicator = $blazar->getDeduplicator();
$template = new XSLTemplate(Config\DEFAULT_LANGUAGE);
$pending_files = $deduplicator->totalPendingFiles();

$template->xml = '<g z="' . $deduplicator->totalHash()
	. '" y="' . $deduplicator->totalBlock()
	. '" x="' . $deduplicator->totalHashLength()
	. '" w="' . Deduplicator\BlockStorage::BLOCK_SIZE
	. '" v="' . $deduplicator->totalBlockSize()
	. '" u="' . $pending_files['File']
	. '" t="' . $pending_files['Size']
	. '">' . $deduplicator->totalLink() . '</g>';

$stats = $blazar->getStats();
$template->rootAttributes = ['z' => $stats['Size'], 'y' => $stats['Files']];
$template->sendXML();