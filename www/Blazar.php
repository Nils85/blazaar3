<?php
use DatabaseToolkit\DatabaseToolkit;

/**
 * Common functions of web application.
 */
class Blazar
{
	private $db;

	public function __construct()
	{
		$this->db = new DatabaseToolkit(Config\DB_SOURCE, Config\DB_USERNAME, Config\DB_PASSWORD,
			[PDO::ATTR_PERSISTENT => true, PDO::ATTR_EMULATE_PREPARES => false, PDO::ATTR_STRINGIFY_FETCHES => false]
			);

		if ($this->db->type == 'sqlite')
		{
			$this->db->pdo->query('PRAGMA journal_mode=OFF');
			$this->db->pdo->query('PRAGMA synchronous=OFF');
		}
	}

	public function getStats()
	{
		$stmt = $this->db->pdo->query('select Files,Size from ' . Config\DB_TABLES_PREFIX . 'Stat');

		if ($stmt == false)  // or null
		{
			$this->createTableStat();
			$stmt = $this->db->pdo->query('select Files,Size from ' . Config\DB_TABLES_PREFIX . 'Stat');
		}

		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	private function createTableStat()
	{
		$type_bigint = $this->db->sql->typeInteger(8);

		$this->db->pdo->exec('create table ' . Config\DB_TABLES_PREFIX
			. 'Stat (Files ' . $type_bigint . ' not null, Size ' . $type_bigint . ' not null)');

		$this->db->pdo->exec('insert into ' . Config\DB_TABLES_PREFIX . 'Stat (Files,Size) values (0,0)');
	}

	public function getDeduplicator()
	{
		return $deduplicator = new Deduplicator\Deduplicator(Config\PATH_FILES, $this->db, Config\DB_TABLES_PREFIX);
	}

	public function incrementFilesStat($size)
	{
		$this->db->pdo->exec('update ' . Config\DB_TABLES_PREFIX . 'Stat set Files=Files+1,Size=Size+' . $size);
	}

	public function decrementFilesStat($size)
	{
		$this->db->pdo->exec('update ' . Config\DB_TABLES_PREFIX . 'Stat set Files=Files-1,Size=Size-' . $size);
	}

	static function getTimeLeft($timestamp)
	{
		if ($timestamp < time())
		{ return '0'; }

		$time_left = $timestamp - time();

		if ($time_left < 60)
		{ return $time_left . ' s'; }

		$time_left = $time_left / 60;

		if ($time_left < 60)
		{ return floor($time_left) . ' m'; }

		return round($time_left / 60) . ' h';
	}
	
	/**
	 * Detect the maximum size for an upload according to the php.ini
	 * @return string Max filesize
	 */
	static function getMaxUploadSize()
	{
		$max_size = ini_get('upload_max_filesize');
		$post_max_size = ini_get('post_max_size');
		$post_max_unit = substr($post_max_size, -1);
		$max_size_unit = substr($max_size, -1);

		if ($max_size_unit === $post_max_unit)
		{
			if ((int)$post_max_size < (int)$max_size)
			{ $max_size = $post_max_size; }
		}
		elseif ($max_size_unit == 'G' && $post_max_unit == 'M')
		{
			$max_size = $post_max_size;
			$max_size_unit = $post_max_unit;
		}

		if ($max_size_unit == 'G' && $max_size != '1G')
		{
			if (PHP_INT_SIZE == 4)  // 32bit CPU
			{ $max_size = '2G'; }

			$max_input_time = ini_get('max_input_time');

			if ($max_input_time == -1)
			{ $max_input_time = ini_get('max_execution_time'); }

			if ($max_input_time > 300)
			{ $max_input_time = 300; }

			if ($max_input_time > 0)
			{
				$max_input_size = (int)ceil($max_input_time / 20);  // max_input_time / 60sec * 3G

				if ($max_input_size < (int)$max_size)
				{ $max_size = $max_input_size . 'G'; }
			}
		}

		return $max_size;
	}
}