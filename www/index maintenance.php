<?php
use XSLTemplate\XSLTemplate;
require 'CONFIG.php';

function __autoload($class_name)
{ require '../' . str_replace('\\', '/', $class_name) . '.php'; }

$blazar = new Blazar();
$stats = $blazar->getStats();

$template = new XSLTemplate(Config\DEFAULT_LANGUAGE);
$template->rootAttributes = ['z' => $stats['Size'], 'y' => $stats['Files']];

$template->xml = '<f z="5"/>';
$template->sendXML();