<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="AA">fr</xsl:variable>
<xsl:variable name="AB">ltr</xsl:variable>
<xsl:variable name="AC">Langue:</xsl:variable>
<xsl:variable name="AD">Sauvegardez ou partagez un nouveau fichier</xsl:variable>
<xsl:variable name="AE">Envoyer</xsl:variable>
<xsl:variable name="AF">espace infini</xsl:variable>
<xsl:variable name="AG">Aide</xsl:variable>
<xsl:variable name="AH">Go</xsl:variable>
<xsl:variable name="AI">Votre fichier est en ligne !</xsl:variable>
<xsl:variable name="AJ">Pour le télécharger copiez l'adresse ci-dessous :</xsl:variable>
<xsl:variable name="AK">Mauvaise clé: verifiez la fin de l'adresse du lien</xsl:variable>
<xsl:variable name="AL">ID invalide: verifiez le début de l'adresse du lien</xsl:variable>
<xsl:variable name="AM">Conservez ce lien précieusement car il sera impossible de le retrouver par la suite.</xsl:variable>
<xsl:variable name="AN">Envoyer un autre fichier</xsl:variable>
<xsl:variable name="AO">Télécharger le fichier</xsl:variable>
<xsl:variable name="AP">Mo</xsl:variable>
<xsl:variable name="AQ">Ko</xsl:variable>
<xsl:variable name="AR">Ce site vous permet de sauvegarder et partager autant de fichiers que vous le souhaitez sans aucune limite dans le temps.</xsl:variable>
<xsl:variable name="AS">Vous trouverez ici les réponses aux questions les plus communes :</xsl:variable>
<xsl:variable name="AT">Comment télécharger un fichier ?</xsl:variable>
<xsl:variable name="AU">Pour chaque fichier envoyé, un lien unique est fourni. Il vous suffit ensuite de copier ce lien puis de la coller dans la barre d'adresse de votre navigateur pour le télécharger. Vous pouvez également coller ce lien directement sur la page d'acceuil du site pour un téléchargement anonyme.</xsl:variable>
<xsl:variable name="AV">Y a t'il vraiment aucune limite ?</xsl:variable>
<xsl:variable name="AW">Comment supprimer un lien ?</xsl:variable>
<xsl:variable name="AX">J'ai perdu mon lien, que faire ?</xsl:variable>
<xsl:variable name="AY">Il n'y a aucun moyen de retrouver l'adresse complète d'un lien. Tout ce que vous pouvez faire c'est renvoyer votre fichier.</xsl:variable>
<xsl:variable name="AZ">Les fichiers stockés sont ils cryptés ?</xsl:variable>
<xsl:variable name="BA">Lorsqu'un fichier est envoyé il est découpé en petits morceaux pour être stocké. Ces blocs de données ne sont pas cryptés mais plutôt mélangés avec des milliards d'autres blocs. Le lien qui permet de reconstituer ce fichier est bien crypté avec une clé contenue dans l'URL que vous seul connaissez. Les autres meta-données comme le nom du fichier sont également cryptés. Il est donc impossible pour toutes personne ayant accès au serveur d'identifier ou d'exploiter les données des utilisateurs.</xsl:variable>
<xsl:variable name="BB">Des informations sont elles conservées quand un fichier est supprimé ?</xsl:variable>
<xsl:variable name="BC">Tout le système est basé sur la "<a href="http://fr.wikipedia.org/wiki/D%C3%A9duplication">déduplication</a>". Cela implique que les données qui constituent un fichier ne peuvent plus être effacées. Seul le lien de téléchargement (permettant de reconstituer un fichier complet) peut être supprimé. Dans ce cas, le fichier sera inaccessible mais la majeure partie de ses données brutes seront toujours stockées.</xsl:variable>
<xsl:variable name="BD">Où se trouve le centre de données qui héberge les fichiers ?</xsl:variable>
<xsl:variable name="BE">Vous avez d'autres questions ?</xsl:variable>
<xsl:variable name="BF">octets</xsl:variable>
<xsl:variable name="BG">Taille max:</xsl:variable>
<xsl:variable name="BH">Signaler un bug</xsl:variable>
<xsl:variable name="BI">Checksums fichier</xsl:variable>
<xsl:variable name="BJ">Fichier introuvable</xsl:variable>
<xsl:variable name="BK">Supprimer un lien définitivement</xsl:variable>
<xsl:variable name="BL">Dupliquer le lien</xsl:variable>
<xsl:variable name="BM">Renommer :</xsl:variable>
<xsl:variable name="BN">Tous les fichiers sont automatiquement cryptés avec une clé de chiffrement</xsl:variable>
<xsl:variable name="BO">La connexion n'est pas sécurisée ! Utilisez plutôt HTTPS pour préserver la confidentialité de vos fichiers</xsl:variable>
<xsl:variable name="BP">Ce service permet-il d'envoyer anonymement des fichiers ?</xsl:variable>
<xsl:variable name="BQ">En utilisant HTTPS votre fournisseur d'accès internet ne peut pas savoir ce que vous envoyez ou téléchargez.</xsl:variable>
<xsl:variable name="BR">Aucune information personnelle n'est demandée aux utilisateurs et il n'y a pas de système d'authentification intégré sur ce site. Cependant l'adresse IP de l'ordinateur ayant envoyé un fichier peut être conservé automatiquement dans les logs du serveur.</xsl:variable>
<xsl:variable name="BS">Téléchargement anonyme</xsl:variable>
<xsl:variable name="BT">Lien :</xsl:variable>
<xsl:variable name="BU">Téléchargez un fichier déjà en ligne</xsl:variable>
<xsl:variable name="BV">Il n'y a pas de limite concernant le nombre de fichier envoyés ou la durée de vie des liens. Cependant la taille d'un fichier ne peut dépasser 4Go.</xsl:variable>
<xsl:variable name="BW">Supprimer</xsl:variable>
<xsl:variable name="BX">Le site est actuellement en cours de maintenance...</xsl:variable>
<xsl:variable name="BY">Projet Blazaar</xsl:variable>
<xsl:variable name="BZ">Annuler</xsl:variable>
<xsl:variable name="CA">Informations du lien</xsl:variable>
<xsl:variable name="CB">Date upload :</xsl:variable>
<xsl:variable name="CC">SUPPRESSION DANS :</xsl:variable>
<xsl:variable name="CD">Téléchargements :</xsl:variable>
<xsl:variable name="CE">Blocs</xsl:variable>
<xsl:variable name="CF">Statistiques globales</xsl:variable>
<xsl:variable name="CG">Flux de données</xsl:variable>
<xsl:variable name="CH">Uploads en attente :</xsl:variable>
<xsl:variable name="CI">Liens actifs :</xsl:variable>
<xsl:variable name="CJ">Fichiers différents :</xsl:variable>
<xsl:variable name="CK">Espace de stockage</xsl:variable>
<xsl:variable name="CL">Données brutes stockées :</xsl:variable>
<xsl:variable name="CM">Après déduplication des fichiers :</xsl:variable>
<xsl:variable name="CN">Après déduplication des blocs :</xsl:variable>
<xsl:variable name="CO">Après compression des blocs :</xsl:variable>
<xsl:variable name="CP">Espace additionel pour identifier les blocs :</xsl:variable>
<xsl:variable name="CQ">Clés</xsl:variable>
<xsl:variable name="CR">Taille de bloc / clé :</xsl:variable>
<xsl:variable name="CS">Disponibilité du serveur ces 30 derniers jours :</xsl:variable>
<xsl:variable name="CT">Il suffit de se rendre sur la page <a href="./delete">delete</a> du site puis de coller votre lien.</xsl:variable>

<xsl:template name="A0"><xsl:param name="z"/><xsl:param name="y"/>
<xsl:value-of select="$z"/> fichiers stockés actuellement (<xsl:value-of select="$y"/>)
</xsl:template>

<xsl:template name="A1"><xsl:param name="z"/>
En <xsl:value-of select="$z"/>.
</xsl:template>

<xsl:template name="A2"><xsl:param name="z"/>
Envoyez un mail à l'adresse <xsl:copy-of select="$z"/>
</xsl:template>

<xsl:template name="A3"><xsl:param name="z"/>
Votre fichier sera supprimé dans <xsl:value-of select="$z"/> heures.
</xsl:template>

<xsl:template name="A4"><xsl:param name="z"/>
Si vous utilisez un vieux navigateur qui ne prend pas en charge HTML5 ou si vous avez désactivé Javascript la taille des fichiers doit être inférieur à <xsl:value-of select="$z"/>o.<br/>Sinon vous pouvez envoyer autant de fichier que vous souhaitez quelle que soit leurs tailles. Tant que personne n'aura supprimé le lien d'un fichier il restera accessible indéfiniment même si il n'est jamais téléchargé.
</xsl:template>

<xsl:template name="A5"><xsl:param name="z"/><xsl:param name="y"/>
Non car l'adresse IP des utilisateurs ayant envoyé un fichier est conservée 1 an avec une partie du lien, la date et l'heure (comme l'impose la loi dans la majorité des pays). C'est également le cas lorsqu'un lien est copié ou supprimé contrairement aux téléchargements qui ne sont pas concernés par ces logs.<br/>
Pour masquer votre adresse IP quand vous envoyez un fichier utilisez une connexion <xsl:copy-of select="$y"/> ou <xsl:copy-of select="$z"/>.
</xsl:template>

<xsl:decimal-format name="d" decimal-separator="," grouping-separator="&#160;"/>
<xsl:variable name="grouping-separator">&#160;</xsl:variable>

<xsl:include href="template.xsl"/>
</xsl:stylesheet>