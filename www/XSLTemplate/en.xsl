<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="AA">en</xsl:variable>
<xsl:variable name="AB">ltr</xsl:variable>
<xsl:variable name="AC">Language:</xsl:variable>
<xsl:variable name="AD">Save or share a new file</xsl:variable>
<xsl:variable name="AE">Upload</xsl:variable>
<xsl:variable name="AF">infinite space</xsl:variable>
<xsl:variable name="AG">Help</xsl:variable>
<xsl:variable name="AH">GB</xsl:variable>
<xsl:variable name="AI">Your file is online!</xsl:variable>
<xsl:variable name="AJ">To download it copy the following address :</xsl:variable>
<xsl:variable name="AK">Wrong key: check the end of link address</xsl:variable>
<xsl:variable name="AL">Invalid ID: check the beginning of link address</xsl:variable>
<xsl:variable name="AM">Save this link because it will be impossible to retrieve them later.</xsl:variable>
<xsl:variable name="AN">Upload another file</xsl:variable>
<xsl:variable name="AO">Download file</xsl:variable>
<xsl:variable name="AP">MB</xsl:variable>
<xsl:variable name="AQ">KB</xsl:variable>
<xsl:variable name="AR">This site allows you to save or share your files online without limit.</xsl:variable>
<xsl:variable name="AS">Here you'll find answers to common questions :</xsl:variable>
<xsl:variable name="AT">How to download a file?</xsl:variable>
<xsl:variable name="AU">For each uploaded file, a unique link is provided. Then just copy this link and paste into your browser to download. You can also paste this link directly on the home page for an anonymous download.</xsl:variable>
<xsl:variable name="AV">Are there really no limits?</xsl:variable>
<xsl:variable name="AW">How to remove a link?</xsl:variable>
<xsl:variable name="AX">I lost the link, what to do?</xsl:variable>
<xsl:variable name="AY">There is no way to retrieve the full adress of a link. All you can do is resend the file (upload will be faster because the file has already been sent)</xsl:variable>
<xsl:variable name="AZ">Stored files are encrypted?</xsl:variable>
<xsl:variable name="BA">When a file is sent it is split into small chunks to be stored. These data blocks are not encrypted but mixed with others. The link that can reconstitute the file is encrypted with a key contained in the URL that only you know.</xsl:variable>
<xsl:variable name="BB">Data are preserved when a file is deleted?</xsl:variable>
<xsl:variable name="BC">The whole system is based on the "<a href="http://en.wikipedia.org/wiki/Data_deduplication">duplication</a>". This implies that data constituting a file can not be erased. Only the download link can be deleted. In this case, the file will not accessible online, but most of its data will always be stored.</xsl:variable>
<xsl:variable name="BD">Where is the datacenter that hosts the files?</xsl:variable>
<xsl:variable name="BE">You have other questions?</xsl:variable>
<xsl:variable name="BF">Bytes</xsl:variable>
<xsl:variable name="BG">Max filesize:</xsl:variable>
<xsl:variable name="BH">Report a bug</xsl:variable>
<xsl:variable name="BI">File checksums</xsl:variable>
<xsl:variable name="BJ">This file doesn't exist</xsl:variable>
<xsl:variable name="BK">Permanently delete a link</xsl:variable>
<xsl:variable name="BL">Duplicate link</xsl:variable>
<xsl:variable name="BM">Rename :</xsl:variable>
<xsl:variable name="BN">All files are automatically encrypted with a cryptographic key</xsl:variable>
<xsl:variable name="BO">The connection is not secure! Consider using HTTPS to preserve confidentiality of your files</xsl:variable>
<xsl:variable name="BP">This service allows it to send files anonymously?</xsl:variable>
<xsl:variable name="BQ">By using HTTPS your internet service provider may not know what you upload or download.</xsl:variable>
<xsl:variable name="BR">Yes because IP addresses are never stored, no personal information is required for users and there is no integrated authentication system. It is therefore impossible for any person with access to the server to identify or exploit user data (except the file name that is not encrypted)</xsl:variable>
<xsl:variable name="BS">Anonymous download</xsl:variable>
<xsl:variable name="BT">Link :</xsl:variable>
<xsl:variable name="BU">Download a file already online</xsl:variable>
<xsl:variable name="BV">There is no limit on the number of files sent or the lifetime of the links. However the size of a file can not exceed 4GB.</xsl:variable>
<xsl:variable name="BW">Delete</xsl:variable>
<xsl:variable name="BX">The site is currently under maintenance...</xsl:variable>
<xsl:variable name="BY">Blazaar project</xsl:variable>
<xsl:variable name="BZ">Cancel</xsl:variable>
<xsl:variable name="CA">Link informations</xsl:variable>
<xsl:variable name="CB">Upload date :</xsl:variable>
<xsl:variable name="CC">DELETION IN :</xsl:variable>
<xsl:variable name="CD">Downloads :</xsl:variable>
<xsl:variable name="CE">Blocks</xsl:variable>
<xsl:variable name="CF">Global statistics</xsl:variable>
<xsl:variable name="CG">Data flow</xsl:variable>
<xsl:variable name="CH">Pending uploads :</xsl:variable>
<xsl:variable name="CI">Active links :</xsl:variable>
<xsl:variable name="CJ">Different files :</xsl:variable>
<xsl:variable name="CK">Storage space</xsl:variable>
<xsl:variable name="CL">Raw data stored :</xsl:variable>
<xsl:variable name="CM">After files deduplication :</xsl:variable>
<xsl:variable name="CN">After blocks deduplication :</xsl:variable>
<xsl:variable name="CO">After blocks compression :</xsl:variable>
<xsl:variable name="CP">Extra space for blocks ID :</xsl:variable>
<xsl:variable name="CQ">Keys</xsl:variable>
<xsl:variable name="CR">Block / Key size :</xsl:variable>
<xsl:variable name="CS">Server availability last 30 days :</xsl:variable>
<xsl:variable name="CT">Just go to the <a href="./delete">delete</a> page of the site and paste your link.</xsl:variable>

<xsl:template name="A0"><xsl:param name="z"/><xsl:param name="y"/>
<xsl:value-of select="$z"/> files currently stored (<xsl:value-of select="$y"/>)
</xsl:template>

<xsl:template name="A1"><xsl:param name="z"/>
In <xsl:value-of select="$z"/>.
</xsl:template>

<xsl:template name="A2"><xsl:param name="z"/>
Send a mail to <xsl:copy-of select="$z"/>
</xsl:template>

<xsl:template name="A3"><xsl:param name="z"/>
Your file will be deleted in <xsl:value-of select="$z"/> hours.
</xsl:template>

<xsl:template name="A4"><xsl:param name="z"/>
If you are using an older browser that does not support HTML5 or if you have disabled Javascript file size must be less than <xsl:value-of select="$z"/>B.<br/>Otherwise you can upload as much as you want regardless of their sizes. As long as no one has removed the link to a file will remain available indefinitely although it is never downloaded.
</xsl:template>

<xsl:template name="A5"><xsl:param name="z"/><xsl:param name="y"/>
No because the IP address of users who sent a file is stored 1 year with part of the link, date and time (as required by law in most countries). This is also the case when a link is copied or deleted contrary to downloads that are not concerned by these logs.<br/>
To hide your IP address when you upload a file use a <xsl:copy-of select="$y"/> connection or <xsl:copy-of select="$z"/>.
</xsl:template>

<xsl:decimal-format name="d" decimal-separator="." grouping-separator=","/>
<xsl:variable name="grouping-separator">,</xsl:variable>

<xsl:include href="template.xsl"/>
</xsl:stylesheet>