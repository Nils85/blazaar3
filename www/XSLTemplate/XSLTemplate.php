<?php
namespace XSLTemplate;

/**
 * Web template system using XSLT 1.0 (compatible with all browsers).
 * Send XML headers with the correct translation (XSL stylesheet)
 */
class XSLTemplate
{
	public $xml = '';
	public $rootAttributes = array();
	private $language;
	private $languages = array();

	public function __construct($default_language = 'en')
	{
		$this->language = $default_language;

		foreach (scandir(__DIR__, SCANDIR_SORT_NONE) as $name)
		{
			if (strlen($name) == 6 && substr($name, -4) == '.xsl')
			{ $this->languages[] = substr($name, 0, 2); }
		}

		if (filter_has_var(INPUT_GET, 'lang'))
		{
			$lang = substr(filter_input(INPUT_GET, 'lang'), 0, 2);

			if (in_array($lang, $this->languages))
			{
				setcookie('lang', $lang);
				$this->language = $lang;
			}
		}
		elseif (filter_has_var(INPUT_COOKIE, 'lang'))
		{
			$this->language = substr(filter_input(INPUT_COOKIE, 'lang'), 0, 2);
		}
		elseif (filter_has_var(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE'))
		{
			foreach (explode(',', filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE')) as $accept_language)
			{
				$lang = substr($accept_language, 0, 2);

				if (in_array($lang, $this->languages))
				{
					$this->language = $lang;
					break;
				}
			}
		}

		if (!in_array($this->language, $this->languages))
		{ $this->language = $this->languages[0]; }
	}

	public function sendXML()
	{
		header('Content-Type: text/xml');

		echo '<?xml version="1.0" encoding="UTF-8"?>';
		echo '<?xml-stylesheet type="text/xsl" href="/XSLTemplate/' , $this->language , '.xsl"?>';
		echo '<xslt';

		foreach ($this->rootAttributes as $key => $value)
		{ echo ' ', $key, '="', $value, '"'; }

		echo '>', $this->xml, '</xslt>';
	}
}