<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:include href="CONFIG.xsl"/>
	<xsl:output method="html" encoding="UTF-8" doctype-system="about:legacy-compat"/>

	<!-- Format number pattern -->
	<xsl:variable name="p" select="concat('###', $grouping-separator, '###')" />

	<!-- Root of all pages -->
	<xsl:template match="xslt">
		<html lang="{$AA}" dir="{$AB}">
			<head>
				<link rel="icon" type="image/png" href="{$B}"/>
				<title><xsl:value-of select="$H"/> - <xsl:value-of select="$AF"/></title>
				<style>
					<xsl:if test="$AB='ltr'">
						html { font-size: 100% }
					</xsl:if>
					<xsl:if test="$AB='rtl'">
						html { font-size: 125% }
						i { font-style: normal }
					</xsl:if>
					html { height: 100% }
					body { height: 100%;
						margin: 0;
						font-size: .9em;
						background: #333 no-repeat center fixed;
						background-image: url(<xsl:value-of select="$C"/>);
						background-size: cover;
						font-family: sans-serif }
					body, a { color: #FFF }
					a { text-decoration: none }
					a:hover { text-decoration: underline }
					abbr { cursor: help }
					.A { margin: 20px;
						font-weight: bold }
					.C:hover { text-decoration: none }
					.D { display: table-row;
						background-color: #000 }
					.E { display: table-cell;
						vertical-align: middle }
					.F { display: inline-block;
						border-radius: 25px;
						padding: 20px 40px;
						margin: 50px auto;
						max-width: 700px;
						background-color: #000 }
					.G { text-align:left; width:75% }
					.G a { text-decoration: underline }
					.H { width:70%;
						border-radius: 25px;
						padding: 50px;
						margin: 99px auto;
						background-color: #000 }
				</style>
				<script>
					function dedup()
					{
						var xhr = new XMLHttpRequest();
						xhr.open('GET', '/dedup.php', true);
						xhr.send(null);
					}

					function enableButton(id)
					{
						document.getElementById(id).style.fontWeight = 'bold';
						document.getElementById(id).removeAttribute('disabled');
					}

					function upload()
					{
						var form = document.getElementById('e');
						var title = document.getElementById('d');
						var progress = document.getElementById('f');
						var tip = document.getElementById('g');

						form.style.borderStyle = 'none';
						title.style.display = 'inline';
						document.getElementById('b').style.display = 'none';  // submit button
						document.getElementById('c').style.display = 'none';  // title label

						var xhr = new XMLHttpRequest();

						if (!(xhr &amp;&amp; ('upload' in xhr) &amp;&amp; ('onprogress' in xhr.upload))
							|| !window.FormData)
						{
							return;
						}

						form.addEventListener('submit', function(e) {
							//prevent regular form posting
							e.preventDefault();

							xhr.upload.addEventListener('progress', function(event) {
								var percent = (100 * event.loaded / event.total);
								progress.innerHTML = percent.toFixed(2) + '%';
							}, false);

							xhr.addEventListener('readystatechange', function(event) {
								if (event.target.readyState == 4 &amp;&amp; event.target.responseText) {
									title.innerHTML = '<xsl:value-of select="$AI"/>';
									tip.innerHTML = '<xsl:value-of select="$AM"/>';
									progress.innerHTML = event.target.responseText
										.replace('#1', "<xsl:value-of select="$AJ"/>")
										.replace('#0', "<xsl:value-of select="$A"/>")
										.replace('#2', "<xsl:value-of select="$AN"/>");
									document.getElementById('a').setAttribute('disabled', '');
									dedup();
								}
							}, false);

							xhr.open('post', '/upload/js.php', true);
							xhr.send(new FormData(this));
						});
					}

					function language(value)
					{
						if (location.href.indexOf('?') > 0 )
						{ location.href += '&amp;lang=' + value; }
						else
						{ location.href = '?lang=' + value; }
					}

					dedup();
				</script>
			</head>
			<body>
				<div style="display:table; height:100%; width:100%; opacity:.7; filter:alpha(opacity=70)">
					<div class="D" style="height:60px"><div class="E">
						<span style="float:right; margin:9px 5%">
							<a href="/info">
								<xsl:call-template name="A0">
									<xsl:with-param name="z" select="format-number(@y, $p, 'd')"/>
									<xsl:with-param name="y">
										<xsl:call-template name="LabelFilesize">
											<xsl:with-param name="size" select="@z"/>
										</xsl:call-template>
									</xsl:with-param>
								</xsl:call-template>
							</a>
						</span>
						<a href="/" class="C">
							<img src="{$B}" style="float:left; margin:3px 9px; border-style:none"/>
							<b><xsl:value-of select="$H"/></b><br/>
							<i><xsl:value-of select="$AF"/></i>
						</a>
					</div></div>
					<div class="E" style="text-align:center">
						<xsl:apply-templates/>
					</div>
					<div class="D" style="height:40px; text-align:center"><div class="E">
						<a href="/project"><xsl:value-of select="$BY"/></a>
						<span style="margin:0 6%">&#160;</span>
						<a href="/help"><xsl:value-of select="$AG"/></a>
						<span style="margin:0 6%">&#160;</span>
						<a href="https://bitbucket.org/Nils85/blazaar3/issues/new" target="blank">
							<xsl:value-of select="$BH"/>
						</a>
						<xsl:if test="not(c)">
							<span style="margin:0 6%">&#160;</span>
							<label for="z" style="margin:0 9px"><xsl:value-of select="$AC"/></label>
							<select id="z" onchange="language(this.value)">
								<xsl:choose>
									<xsl:when test="$AA='en'">
										<option value="en">English</option>
										<option value="fr">Français</option>
									</xsl:when>
									<xsl:otherwise>
										<option value="fr">Français</option>
										<option value="en">English</option>
									</xsl:otherwise>
								</xsl:choose>
							</select>
						</xsl:if>
					</div></div>
				</div>
			</body>
		</html>
	</xsl:template>

	<!-- Page index?dl=... -->
	<xsl:template match="a">
		<div class="F">
			<form method="post">
				<label for="c">Code:</label>
				<input id="c" type="password" name="code" style="width:200px; margin:9px" autofocus=""/>
				<br/><br/>
				<input type="submit" value="Go"/>
			</form>
		</div>
	</xsl:template>

	<!-- Page index -->
	<xsl:template match="b">
		<div class="F" style="padding:20px">
			<form action="/upload/" method="post" enctype="multipart/form-data" id="e"
					style="border-style:dashed; border-radius:9px; margin-bottom:9px; padding:20px 30px">
				<label for="a" id="c" style="font-size:2em"><xsl:value-of select="$AD"/></label>
				<span id="d" style="font-size:2em; display:none">Upload...<img src="{$E}"/></span>
				<br/><br/>
				<input type="file" id="a" name="up[]" onchange="enableButton('b')"/>
				<input type="submit" id="b" value="{$AE}" style="margin:0 9px" onclick="upload()"/>
				<br/>
				<span id="f"><xsl:value-of select="concat($BG,' ',@z)"/></span>
			</form>
			<span id="g" style="font-size:.7em">
				<xsl:choose>
					<xsl:when test=".!='off' and starts-with($A,'https')">
						<a href="{$A}"><xsl:value-of select="$BO"/></a>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$BN"/>
					</xsl:otherwise>
				</xsl:choose>
			</span>
			<script>document.getElementById('b').setAttribute('disabled', '')</script>
		</div>
	</xsl:template>

	<!-- Page upload (without javascrit) -->
	<xsl:template match="c">
		<div class="F">
			<span style="font-size:2em"><xsl:value-of select="$AI"/></span><br/>
			<i><xsl:value-of select="."/></i><br/>
			<p>
				<xsl:value-of select="$AJ"/><br/>
				<input type="text" value="{$A}?dl={@z}" style="width:250px" readonly="" onclick="this.select()"/>
				<br/>Code:
				<input type="text" value="{@y}" style="width:250px" readonly="" onclick="this.select()"/>
				<br/>
				<span style="font-size:.7em"><xsl:value-of select="$AM"/></span>
			</p>
			<a href="/upload/" style="text-decoration:underline"><xsl:value-of select="$AN"/></a>
		</div>
	</xsl:template>

	<!-- Page index?dl=... + POST code -->
	<xsl:template match="d">
		<div class="F">
			<xsl:call-template name="FileInfo">
				<xsl:with-param name="Name" select="."/>
				<xsl:with-param name="Size" select="@y"/>
				<xsl:with-param name="UploadDate" select="@x"/>
				<xsl:with-param name="MD5" select="@t"/>
				<xsl:with-param name="SHA1" select="@u"/>
				<xsl:with-param name="SHA2" select="@v"/>
				<xsl:with-param name="DownloadCounter" select="@w"/>
				<xsl:with-param name="RemoveTimer" select="@s"/>
				<xsl:with-param name="AccessTimer" select="@q"/>
			</xsl:call-template>
			<form method="post" action="/download/" style="display:inline-block; margin:9px">
				<input type="hidden" name="dl" value="{@z}"/>
				<input type="hidden" name="code" value="{@r}"/>
				<input type="submit" value="{$AO}"/>
			</form>
			<form method="post" action="/copy/" style="display:inline-block; margin:9px">
				<input type="hidden" name="link" value="{@z}"/>
				<input type="hidden" name="code" value="{@r}"/>
				<input type="submit" value="{$BL}"/>
			</form>
		</div>
	</xsl:template>

	<!-- Page help -->
	<xsl:template match="e">
		<div class="H">
			<span style="font-size:2em"><xsl:value-of select="$AG"/></span><br/><br/>
			<xsl:value-of select="$AR"/><br/>
			<xsl:value-of select="$AS"/><br/>
			<br/>
			<a href="#question1"><xsl:value-of select="$AT"/></a><br/>
			<a href="#question2"><xsl:value-of select="$AV"/></a><br/>
			<a href="#question3"><xsl:value-of select="$AW"/></a><br/>
			<a href="#question4"><xsl:value-of select="$AX"/></a><br/>
			<a href="#question5"><xsl:value-of select="$AZ"/></a><br/>
			<a href="#question6"><xsl:value-of select="$BB"/></a><br/>
			<xsl:if test="$F"><a href="#question7"><xsl:value-of select="$BD"/></a><br/></xsl:if>
			<a href="#question8"><xsl:value-of select="$BP"/></a><br/>
			<a href="#question9"><xsl:value-of select="$BE"/></a><br/>
			<a name="question1"/>
		</div>
		<div class="H">
			<span style="font-size:2em"><xsl:value-of select="$AT"/></span>
			<br/><br/><xsl:value-of select="$AU"/><br/>
			<a name="question2"/>
		</div>
		<div class="H">
			<span style="font-size:2em"><xsl:value-of select="$AV"/></span>
			<br/><br/><xsl:value-of select="$BV"/><br/>
			<a name="question3"/>
		</div>
		<div class="H">
			<span style="font-size:2em"><xsl:value-of select="$AW"/></span>
			<br/><br/><xsl:copy-of select="$CT"/><br/>
			<a name="question4"/>
		</div>
		<div class="H">
			<span style="font-size:2em"><xsl:value-of select="$AX"/></span>
			<br/><br/><xsl:value-of select="$AY"/><br/>
			<a name="question5"/>
		</div>
		<div class="H">
			<span style="font-size:2em"><xsl:value-of select="$AZ"/></span>
			<br/><br/><xsl:value-of select="$BA"/><br/>
			<a name="question6"/>
		</div>
		<div class="H">
			<span style="font-size:2em"><xsl:value-of select="$BB"/></span>
			<br/><br/><xsl:copy-of select="$BC"/><br/>
			<a name="question7"/>
		</div>
		<xsl:if test="$F">
			<div class="H">
				<span style="font-size:2em"><xsl:value-of select="$BD"/></span>
				<br/><br/><xsl:value-of select="$F"/><br/><br/>
				<a name="question8"/>
			</div>
		</xsl:if>
		<div class="H">
			<span style="font-size:2em"><xsl:value-of select="$BP"/></span>
			<br/><br/><xsl:value-of select="$BR"/><br/>
			<a name="question9"/>
		</div>
		<div class="H">
			<span style="font-size:2em"><xsl:value-of select="$BE"/></span>
			<br/><br/>
			<xsl:call-template name="A2">
				<xsl:with-param name="z" select="$G"/>
			</xsl:call-template>
			<br/><br/>
		</div>
	</xsl:template>

	<!-- Messages error -->
	<xsl:template match="f">
		<div class="F" style="padding:50px 90px">
			<xsl:if test=".!=''"><span style="font-size:2em"><xsl:value-of select="."/></span><br/></xsl:if>
			<xsl:if test="@z=2"><br/><xsl:value-of select="$BJ"/></xsl:if>
			<xsl:if test="@z=3"><br/><xsl:value-of select="$AK"/></xsl:if>
			<xsl:if test="@z=4"><br/><xsl:value-of select="$AL"/></xsl:if>
			<xsl:if test="@z=5"><br/><xsl:value-of select="$BX"/></xsl:if>
		</div>
	</xsl:template>

	<!-- Page info -->
	<xsl:template match="g">
		<div class="F">
			<span style="font-size:2em"><xsl:value-of select="$CF"/></span>
			<br/><br/>
			<table style="border-spacing: 20px 5px">
				<thead style="text-align:left">
					<tr>
						<th colspan="3"><xsl:value-of select="$CG"/></th>
					</tr>
				</thead>
				<tbody style="text-align:right">
					<tr>
						<td><xsl:value-of select="$CH"/></td>
						<td><xsl:value-of select="@u"/></td>
						<td>
							(<xsl:call-template name="LabelFilesize">
								<xsl:with-param name="size" select="@t"/>
							</xsl:call-template>)
						</td>
					</tr>
					<tr>
						<td><xsl:value-of select="$CI"/></td>
						<td><xsl:value-of select="format-number(., $p, 'd')"/></td>
						<td>
							(<xsl:call-template name="LabelFilesize">
								<xsl:with-param name="size" select="/xslt/@z"/>
							</xsl:call-template>)
						</td>
					</tr>
					<tr>
						<td><xsl:value-of select="$CJ"/></td>
						<td><xsl:value-of
							select="format-number(@z, $p, 'd')"/>
						</td>
						<td>
							(~ <xsl:call-template name="LabelFilesize">
								<xsl:with-param name="size" select="(@x*@w)-(@z*(@w div 2))"/>
							</xsl:call-template>)
						</td>
					</tr>
				</tbody>
			</table>
			<br/>
			<table style="border-spacing: 20px 5px">
				<thead style="text-align:left">
					<tr>
						<th colspan="3"><xsl:value-of select="$CK"/></th>
					</tr>
				</thead>
				<tbody style="text-align:right">
					<tr>
						<td><xsl:value-of select="$CL"/></td>
						<td>
							<abbr title="{format-number(/xslt/@z, $p, 'd')} {$BF}">
								<xsl:call-template name="LabelFilesize">
									<xsl:with-param name="size" select="/xslt/@z"/>
								</xsl:call-template>
							</abbr>
						</td>
						<td></td>
					</tr>
					<tr>
						<td><xsl:value-of select="$CM"/></td>
						<td>
							<xsl:variable name="byte1" select="(@x*@w)-(@z*(@w div 2))"/>
							~ <abbr title="{format-number($byte1, $p, 'd')} {$BF}">
								<xsl:call-template name="LabelFilesize">
									<xsl:with-param name="size" select="$byte1"/>
								</xsl:call-template>
							</abbr>
						</td>
						<td><xsl:value-of select="format-number(@x, $p, 'd')"/>&#160;<xsl:value-of select="$CE"/></td>
					</tr>
					<tr>
						<td><xsl:value-of select="$CN"/></td>
						<td>
							<xsl:variable name="byte2" select="(@y*@w)-(@z*(@w div 2))"/>
							~ <abbr title="{format-number($byte2, $p, 'd')} {$BF}">
								<xsl:call-template name="LabelFilesize">
									<xsl:with-param name="size" select="$byte2"/>
								</xsl:call-template>
							</abbr>
						</td>
						<td>
							<xsl:value-of select="format-number(@y, $p, 'd')"/>&#160;<xsl:value-of select="$CE"/>
						</td>
					</tr>
					<tr>
						<td><xsl:value-of select="$CO"/></td>
						<td>
							<abbr title="{format-number(@v, $p, 'd')} {$BF}">
								<xsl:call-template name="LabelFilesize">
									<xsl:with-param name="size" select="@v"/>
								</xsl:call-template>
							</abbr>
						</td>
						<td></td>
					</tr>
					<tr>
						<td><xsl:value-of select="$CP"/></td>
						<td>
							<xsl:variable name="byte3" select="(@x*12)+(@y*7)"/>
							+ <abbr title="{format-number($byte3, $p, 'd')} {$BF}">
								<xsl:call-template name="LabelFilesize">
									<xsl:with-param name="size" select="$byte3"/>
								</xsl:call-template>
							</abbr>
						</td>
						<td><xsl:value-of select="format-number(@x+@y, $p, 'd')"
							/>&#160;<xsl:value-of select="$CQ"/>
						</td>
					</tr>
					<tr>
						<td><xsl:value-of select="$CR"/></td>
						<td><xsl:value-of select="format-number(@w, $p, 'd')"
							/>&#160;<xsl:value-of select="$BF"/>
						</td>
						<td>12 <xsl:value-of select="$BF"/></td>
					</tr>
				</tbody>
			</table>
			<br/><br/>
			<xsl:if test="$D">
				<xsl:value-of select="$CS"/><br/>
				<a href="https://www.statuscake.com" title="Website Uptime Monitoring">
					<img src="https://app.statuscake.com/button/index.php?Track={$D}&amp;Days=30&amp;Design=2"/>
				</a>
			</xsl:if>
		</div>
	</xsl:template>

	<!-- Page copy -->
	<xsl:template match="h">
		<div class="F">
			<span style="font-size:1.5em"><xsl:value-of select="."/></span><br/>
			<xsl:call-template name="LabelFilesize"><xsl:with-param name="size" select="@y"/></xsl:call-template>
			<br/>
			<br/>
			<form method="post" action="/copy/">
				<label for="c"><xsl:value-of select="$BM"/></label>
				<input id="c" style="width:57%; margin:9px" type="text" name="name" value="{.}"/>
				<br/><br/>
				<input type="hidden" name="link" value="{@z}"/>
				<input type="hidden" name="code" value="{@x}"/>
				<input type="submit" value="{$BL}"/>
			</form>
			<a href="/?dl={@z}"><xsl:value-of select="$BZ"/></a>
		</div>
	</xsl:template>

	<!-- Page delete + POST -->
	<xsl:template match="i">
		<div class="F">
			<xsl:call-template name="FileInfo">
				<xsl:with-param name="Name" select="."/>
				<xsl:with-param name="Size" select="@y"/>
				<xsl:with-param name="UploadDate" select="@x"/>
				<xsl:with-param name="MD5" select="@t"/>
				<xsl:with-param name="SHA1" select="@u"/>
				<xsl:with-param name="SHA2" select="@v"/>
				<xsl:with-param name="DownloadCounter" select="@w"/>
				<xsl:with-param name="RemoveTimer" select="@s"/>
				<xsl:with-param name="AccessTimer" select="@q"/>
			</xsl:call-template>
			<form method="post" action="/delete/">
				<input type="hidden" name="drop" value="{@z}"/>
				<input type="hidden" name="code" value="{@r}"/>
				<input type="submit" value="{$BW}"/>
			</form>
		</div>
	</xsl:template>

	<!-- Page download (no POST) -->
	<xsl:template match="j">
		<br/>
		<div id="x" class="F" style="padding:30px">
			<form action="/download/" method="post" enctype="multipart/form-data" id="e">
				<label for="a" id="c" style="font-size:2em"><xsl:value-of select="$BU"/></label>
				<br/><br/>
				<xsl:value-of select="$BT"/>
				<input type="text" name="link" value="" style="width:250px;margin:9px"/>
				<br/>Code :
				<input id="c" type="password" name="code" style="width:250px; margin:9px"/>
				<br/>
				<input type="submit" id="b" value="{$BS}"/>
				<span id="f" style="font-size:2em; display:none"></span>
			</form>
		</div>
	</xsl:template>

	<!-- Page delete -->
	<xsl:template match="k">
		<br/>
		<div class="F" style="padding:30px">
			<form action="/delete/" method="post" enctype="multipart/form-data" id="e">
				<label for="a" id="c" style="font-size:2em"><xsl:value-of select="$BK"/></label>
				<br/><br/>
				<xsl:value-of select="$BT"/>
				<input type="text" name="link" value="" style="width:250px;margin:9px"/>
				<br/>Code :
				<input type="password" name="code" value="" style="width:250px;margin:9px"/>
				<br/>
				<input type="submit" id="b" value="{$BW}"/>
				<span id="f" style="font-size:2em; display:none"></span>
			</form>
		</div>
	</xsl:template>

	<!-- Page project -->
	<xsl:template match="l">
		<div class="F">
			This website is powered by<br/>
			<span style="font-size:2em">Blazaar V351</span>
			<br/>(stable version)<br/><br/>
			<br/>Released under <a href="https://bitbucket.org/Nils85/blazaar3/src/master/LICENSE.txt">GNU Affero General Public License v3.0</a> (AGPL)<br/><br/>
			<br/>Project source :
			<br/><a href="https://bitbucket.org/Nils85/blazaar3">https://bitbucket.org/Nils85/blazaar3</a>
		</div>
	</xsl:template>

	<!-- Messages -->
	<xsl:template match="m">
		<div class="F" style="padding:50px 90px">
			<xsl:if test="@z=0"><xsl:value-of select="."/></xsl:if>
			<xsl:if test="@z=1">
				<xsl:call-template name="A3"><xsl:with-param name="z" select="."/></xsl:call-template>
			</xsl:if>
		</div>
	</xsl:template>

	<!-- Components -->

	<xsl:template name="LabelFilesize"><xsl:param name="size"/>
		<xsl:choose>
			<xsl:when test="$size > 1073741823">
				<xsl:value-of select="round($size div 1073741824)"/>&#160;<xsl:value-of select="$AH"/>
			</xsl:when>
			<xsl:when test="$size > 1048575">
				<xsl:value-of select="round($size div 1048576)"/>&#160;<xsl:value-of select="$AP"/>
			</xsl:when>
			<xsl:when test="$size > 1023">
				<xsl:value-of select="round($size div 1024)"/>&#160;<xsl:value-of select="$AQ"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$size"/>&#160;<xsl:value-of select="$BF"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="FileInfo">
		<xsl:param name="Name"/>
		<xsl:param name="Size"/>
		<xsl:param name="UploadDate"/>
		<xsl:param name="MD5"/>
		<xsl:param name="SHA1"/>
		<xsl:param name="SHA2"/>
		<xsl:param name="DownloadCounter"/>
		<xsl:param name="RemoveTimer"/>
		<xsl:param name="AccessTimer"/>
		<xsl:choose>
			<xsl:when test="string-length($Name) > 99">
				<span style="font-size:1.3em"><xsl:value-of select="$Name"/></span>
			</xsl:when>
			<xsl:when test="string-length($Name) > 50">
				<span style="font-size:1.4em"><xsl:value-of select="$Name"/></span>
			</xsl:when>
			<xsl:otherwise>
				<span style="font-size:1.5em"><xsl:value-of select="$Name"/></span>
			</xsl:otherwise>
		</xsl:choose>
		<br/>
		<abbr title="{format-number($Size, $p, 'd')} {$BF}">
			<xsl:call-template name="LabelFilesize"><xsl:with-param name="size" select="$Size"/></xsl:call-template>
		</abbr>
		<table style="margin: 20px auto; border-spacing: 20px 4px">
			<thead style="text-align:left">
				<tr><th colspan="2"><xsl:value-of select="$CA"/></th></tr>
			</thead>
			<tbody style="text-align:right">
				<tr>
					<td><xsl:value-of select="$CD"/></td>
					<td><xsl:value-of select="$DownloadCounter"/></td>
				</tr>
				<tr>
					<td><xsl:value-of select="$CB"/></td>
					<td>
						<abbr title="{translate($UploadDate,'T',' ')} (UTC)">
							<xsl:value-of select="substring-before($UploadDate,'T')"/>
						</abbr>
					</td>
				</tr>
				<tr>
					<td>Last access :</td>
					<td>
						<abbr title="{translate($AccessTimer,'T',' ')} (UTC)">
							<xsl:value-of select="substring-before($AccessTimer,'T')"/>
						</abbr>
					</td>
				</tr>
				<xsl:if test="$RemoveTimer != '0'">
					<tr>
						<td><xsl:value-of select="$CC"/></td>
						<td><b><xsl:value-of select="$RemoveTimer"/></b></td>
					</tr>
				</xsl:if>
			</tbody>
		</table>
		<table style="text-align:left; margin: 0 auto 20px auto; border-spacing: 9px 4px; width:80%">
			<thead>
				<tr><th colspan="2"><xsl:value-of select="$BI"/></th></tr>
			</thead>
			<tbody>
				<xsl:if test="$MD5"><tr>
					<td style="text-align:right; width:60px">MD5 :</td>
					<td><input type="text" value="{$MD5}" style="width:99%" readonly=""/></td>
				</tr></xsl:if>
				<xsl:if test="$SHA1"><tr>
					<td style="text-align:right">SHA1 :</td>
					<td><input type="text" value="{$SHA1}" style="width:99%" readonly=""/></td>
				</tr></xsl:if>
				<tr>
					<td style="text-align:right">SHA256 :</td>
					<td><input type="text" value="{$SHA2}" style="width:99%" readonly=""/></td>
				</tr>
			</tbody>
		</table>
	</xsl:template>

</xsl:stylesheet>